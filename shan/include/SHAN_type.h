/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef SHAN_TYPE_H
#define SHAN_TYPE_H

#include <stdio.h>
#include <stdlib.h>

#include "SHAN_segment.h"
#include "SHAN_comm.h"

#ifdef __cplusplus
extern "C"
{
#endif

/** \file SHAN_type.h
 *  \brief SHAN_type header. Type conversion in shared memory.
 *   
 */

/** SHAN layout types
 */
enum shan_layout_type  {
    SHAN_CHAR_AoS_LAYOUT = 0, 
    SHAN_INT_AoS_LAYOUT,
    SHAN_LONG_AoS_LAYOUT,
    SHAN_FLOAT_AoS_LAYOUT,
    SHAN_DOUBLE_AoS_LAYOUT,
    MAX_LAYOUTS,
    SHAN_CHAR_SoA_LAYOUT = MAX_LAYOUTS,
    SHAN_INT_SoA_LAYOUT,
    SHAN_LONG_SoA_LAYOUT,
    SHAN_FLOAT_SoA_LAYOUT,
    SHAN_DOUBLE_SoA_LAYOUT
};

  
/** Converts shared mem send type in shared mem recv type.
 *  Finalizes receive in shared mem.
 * 
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment    - used data segment
 * @param type_id         - used type id
 * @param idx             - rank index in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
void shan_comm_write_local(shan_neighborhood_t *neighborhood_id
			 , shan_segment_t *data_segment
			 , const int type_id
			 , const int idx
    );

/** Converts shared mem send type in shared mem recv type.
 *  Reverse direction of shan_comm_write_local.
 *  Finalizes receive in shared mem.
 * 
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment    - used data segment
 * @param type_id         - used type id
 * @param idx             - rank index in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
void shan_comm_get_local(shan_neighborhood_t *neighborhood_id
			 , shan_segment_t *data_segment
			 , const int type_id
			 , const int idx
    );

/** Finalizes receive for remote comm
 *
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment    - used data segment
 * @param type_id         - used type id
 * @param buffer_id         - used buffer id
 * @param idx             - rank index in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
void shan_comm_get_remote(shan_neighborhood_t *neighborhood_id
			  , shan_segment_t *const data_segment
			  , int const type_id
			  , int const buffer_id
			  , int const idx
     );

/** Returns type data structure for node local ranks
 *  
 * @param type_info       - type data struct (SHAN_comm.h)
 * @param neighborhood_id - general neighborhood handle
 * @param local_rank      - node local rank 
 * @param num_neighbors   - number of neighbors
 * @param type_id         - used type id
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */  
int shan_get_shared_type(type_local_t *type_info
			  , shan_neighborhood_t *neighborhood_id
			  , int local_rank
			  , int num_neighbors
			  , int type_id
     );
    
    
/** Gets type data structure for node local ranks
 *  
 * @param neighborhood_id    - general neighborhood handle
 * @param type_id            - used type id
 * @param nelem_send         - pointer to number of send elements in shared mem
 * @param nelem_recv         - pointer to number of recv elements in shared mem
 * @param nelem_per_send     - pointer to number of data types per send element in shared mem
 * @param nelem_per_recv     - pointer to number of data types per recv element in shared mem
 * @param send_offset_index  - pointer to offset index of send elements in shared mem
 * @param recv_offset_index  - pointer to offset index of recv elements in shared mem
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_type_layout(shan_neighborhood_t *neighborhood_id
		     , int type_id
		     , int **nelem_send
		     , int **nelem_recv
		     , int **nelem_per_send
		     , int **nelem_per_recv
		     , long **send_offset_index
		     , long **recv_offset_index
    );

/** Getter function for type data
 *  
 * @param type_info       - type data struct (SHAN_comm.h)   
 * @param shm_ptr         - pointer to shared memory 
 * @param num_neighbors   - rank local number of neighbors in neighborhood
 * @param type_element    - type element
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_get_type(type_local_t *type_info
			, void *shm_ptr
			, int num_neighbors
			, shan_element_t *type_element
     );


/** Setter function for type layout
 *  
 * @param data_segment     - used data segment
 * @param d1               - primary array dim (fast index)
 * @param d2               - secondary array dim
 * @param type_layout      - AoS = 0-4 (char = 0,int = 1,long = 2,float= 3,double = 4)
 *                           SoA = 5-9 (char = 5,int = 6,long = 7,float= 8,double = 9), default = 0
 */
void shan_set_data_layout(shan_segment_t *data_segment
			  , long const d1
			  , long const d2
			  , int const type_layout
    );
    

/** Validation of mutual number of send/recv elements
 *                       number of elements per item
 *  
 * @param neighborhood_id  - general neighborhood handle
 * @param type_id          - used type id
 *
 */
void shan_type_validate(shan_neighborhood_t * const neighborhood_id
			, int type_id
    );


#ifdef __cplusplus
}
#endif

#endif


