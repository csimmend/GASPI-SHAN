/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef SHAN_segment_H
#define SHAN_segment_H

#include <mpi.h>

#ifdef __cplusplus
extern "C"
{
#endif

/** \file SHAN_segment.h
    \brief SHAN_segment header for notifications in shared memory.

    The SHAN (SHA_red N_otifications) interface is a user-level 
    API which aims at migrating flat MPI (legacy) code towards 
    an asynchronous dataflow model.  SHAN uses the GASPI API 
    and extends ideas from MPI shared windows.

    GASPI is a PGAS communication library which is based on the 
    concept of one-sided, notified communication. The synchronization 
    context here is bundled together with a one-sided message such that 
    a communication target becomes able to test for completion of the 
    received one-sided communication.

    Traditionally the GASPI programming model has been aimed at 
    multithreaded or task-based applications. In GASPI the synchronization
    context is bundled together with a one-sided message such that
    a communication target becomes able to test for completion of the
    received one-sided communication.

    In order to support a migration of legacy applications 
    (with a flat MPI communication model)  towards GASPI, we have extended 
    the concept of shared MPI windows towards a notified communication model 
    in which the processes sharing a common window become able to see all 
    one-sided and notified communication targeted at this window.
    Similarly we have extended the concept of MPI shared windows with
    shared notifications, which are globally visible in shared memory.

    Besides the possibility to entirely avoid node-internal communication 
    and to make use of a much improved overlap of communication and 
    computation the model of notified communication in GASPI shared windows
    will allow legacy SPMD applications a transition towards 
    an asynchronous dataflow model.
*/

#define MAX_SEGMENT 512 //!< max number of segments 

/** SHAN maintains two different shared mem segments  for
 *  1. actual solver data
 *  2. data types 
 */
enum shan_type  {
     SHAN_DATA   = 0, 
     SHAN_TYPE   = 1,
     SHAN_REMOTE = 2,
};
    

/** SHAN return values 
 */
enum shan_return_val  {
    SHAN_ERROR   = -2, 
    SHAN_FAIL    = -1,
    SHAN_SUCCESS =  0
};
    

/** SHAN shared mem notifications
 */
enum shan_notify  {
     SHAN_NOTIFY       = 0, 
     SHAN_READ         = 1,
     SHAN_HAVE_READ    = 2,
     SHAN_WRITE        = 3,
     SHAN_HAVE_WRITTEN = 4,
     MAX_SHARED_NOTIFICATION
};

/** 64 byte aligned notifications struct for shared mem notifications.
 */
typedef struct
{
    volatile int val  __attribute__((aligned(64))); //!< notification value
} shan_notification_t;
    

/** Segment tracking struct, holds the var name
*/
typedef struct
{
    char var_name[80];  //!< variable name
} shan_track_t;


/** Segment layout struct, shared, holds the segment layout
 *
 * AoS = 0-4 (char = 0,int = 1,long = 2,float= 3,double = 4), SoA = 5-9
 *  - AoS packs d1 elements per index 
 *  - SoA packs a repeated type (d2 times) with 1 element per index  
 */
typedef struct
{
    long localSz;               //!< (process) local segment size
    long d1;                    //!< primary array dimension
    long d2;                    //!< secondary array dimension
    int segment_id;             //!< segment_id
    int type_layout;            //!< AoS = 0-4 (char = 0,int = 1,long = 2,float= 3,double = 4), SoA = 5-9 
} shan_layout_t;

/** Segment struct, shared, holds all segment information.
 */
typedef struct
{
    int shan_id;                //!< shared segment id

#ifndef __cplusplus
    void **restrict ptr_array;  //!< shan ptr array
    void *restrict shan_ptr;    //!< local segment pointer    
#else
    void **ptr_array;           //!< shan ptr array
    void *shan_ptr;             //!< local segment pointer    
#endif

    int shan_type;              //!< shared segment type
    long dataSz;                //!< segment size
    long *localDataSz;          //!< segment size array
    MPI_Comm MPI_COMM_SHM;      //!< MPI shared mem communicator  

    int *fd;                    //!< shmem file descriptor array
    int  shmid;                 //!< shmem id
    MPI_Win segment_win;        //!< window handle
    char shan_domain_name[80];  //!< unique shmem name
    
} shan_segment_t;


/** Local allocation of shared memory of size dataSz
 *     
 * Note: Memory will be page-aligned.
 *
 * @param segment      - segment handle
 * @param shan_id      - (unique) segment id
 * @param shan_type    - type of allocated memory 
 * @param dataSz       - required memory size per rank in byte
 * @param MPI_COMM_SHM - shared mem communicator
 *
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
int shan_alloc_shared(shan_segment_t *const segment
		      , const int shan_id
		      , const int shan_type
		      , const long dataSz
		      , const MPI_Comm MPI_COMM_SHM 
	);

/** Free shared memory.
 *     
 * @param segment      - segment handle
 *
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
int shan_free_shared(shan_segment_t * const segment);


/** Gets segment_id from shared mem ptr.
 *     
 * @param shm_ptr      - shared mem pointer
 *
 * @returns segment_id.
 */
int shan_get_segment_id(void *shm_ptr);


/** Gets type_layout from shared mem ptr.
 *     
 * @param shm_ptr      - shared mem pointer
 *
 * @returns type_layout.
 */
int shan_get_type_layout(void *shm_ptr);


/** Sets type_layout from shared mem ptr.
 *     
 * @param shm_ptr     - shared mem pointer
 * @param layout      - type layout (in)
 *
 */
void shan_set_type_layout(void *shm_ptr
			  , int layout
    );

/** Gets array dimensions from shared mem ptr.
 *     
 * @param shm_ptr     - shared mem pointer
 * @param d1          - primary array dimension
 * @param d2          - secondary array dimension
 *
 */
void shan_get_array_dimension(void *shm_ptr
			      , long *d1
			      , long *d2
    );


/** Sets array dimensions from shared mem ptr.
 *     
 * @param shm_ptr     - shared mem pointer
 * @param d1          - primary array dimension
 * @param d2          - secondary array dimension
 *
 */
void shan_set_array_dimension(void *shm_ptr
			      , long d1
			      , long d2
    );

/** Gets shared mem ptr.
 *     
 * @param segment      - segment handle
 * @param rank         - local rank id
 * @param shm_ptr      - shared mem pointer
 *
 * @returns type_layout.
 */
int shan_get_shared_ptr(shan_segment_t * const segment
			, const int rank
			, void **shm_ptr			  
    );  
    
/** Returns default size for the SHAN mem pool  
 *  
 * Configurable via GASPI_SHAN_MEM_SHARE as a percentage 
 * of available total node memory. (Defaults to 0.1 = 10%) 
 *
 * @param MPI_COMM_SHM - shared mem communicator
 * 
 * @return Size of SHAN mem pool.
 */
long shan_alloc_get_default_mem_sz(const MPI_Comm MPI_COMM_SHM);



/** Increments shared mem notfication.
 *  Sets write fence such that local result is valid, 
 *  once the incremented value is visible for other local ranks.
 *
 * @param ptr - pointer to shared notification array
 * @param idx - shared mem notification id
 * @param increment - increment value
 * 
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
static inline 
int shan_notify_increment_shared(shan_notification_t *const ptr
				 , const int idx
				 , const int increment
				 )
{  
  volatile shan_notification_t *nid = ptr + idx;

  __sync_synchronize();
  volatile int res = __sync_add_and_fetch(&(nid->val)
					  , increment
      );

  return SHAN_SUCCESS; 
}



/** Compare swap wrapper
 *
 * @param ptr  - pointer to shared notification array
 * @param idx  - shared mem notification id
 * @param rval - old increment value
 * 
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
static inline 
int shan_notify_compare_swap(shan_notification_t *const ptr  
			     , int const idx
			     , int const rval 
    )
{
    volatile shan_notification_t *nid = ptr + idx;
    int val = -1;
    if (nid->val == rval)
    {
	val = __sync_val_compare_and_swap (&(nid->val)
					   , rval
					   , rval+1
	    );
    }
    return (val == rval) ? SHAN_SUCCESS : -1;
}

/** Resets shared mem notification.
 *
 * @param ptr - pointer to shared notification array
 * @param idx - shared mem notification id
 * @param val - old value of the notification
 *
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
static inline 
int shan_notify_reset_shared(shan_notification_t *const ptr
			     , const int idx
			     , int * const val)
{
  volatile shan_notification_t *nid = ptr + idx;
  volatile int res = __sync_val_compare_and_swap (&(nid->val)
						  , nid->val
						  , 0
      );

  *val =  res;
  return SHAN_SUCCESS;
}

/** Inits shared mem notfication.
 *
 * @param ptr - pointer to shared notification array
 * @param idx - shared mem notification id
 * 
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
static inline 
int shan_notify_init_shared(shan_notification_t *const ptr
			    , const int idx
			    , const int val
			    )
{
  volatile shan_notification_t *nid = ptr + idx;
  nid->val = val;

  return SHAN_SUCCESS;
}

    
/** Tests for shared mem notfication.
 *
 * @param ptr - pointer to shared notification array
 * @param idx - shared mem notification id
 * @param val - current notification value
 * 
 * @return SHAN_SUCCESS in case of success, SHAN_ERROR in case of error.
 */
static inline 
int shan_notify_test_shared(shan_notification_t *const ptr
			    , const int idx
			    , int * const val
			    )
{
  volatile shan_notification_t *nid = ptr + idx;
  volatile int res = nid->val;
  
  *val =  res;

  return SHAN_SUCCESS;
}


/** Malloc, tracks pointer name 
 *
 * @param var_name - variable name
 * @param bytes    - malloc sz
 * @param ptr      - pointer to var
 */
void shan_track_malloc(char* var_name
		       , long bytes
		       , void **ptr
    );


/** Prints pointer name
 *
 * @param ptr - pointer to var 
 */
void shan_track_ptr_name(void *ptr);


#ifdef __cplusplus
}
#endif

#endif
