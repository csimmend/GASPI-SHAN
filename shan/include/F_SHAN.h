/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef F_SHAN_H
#define F_SHAN_H

#include <mpi.h>
#include "SHAN_segment.h"


#ifdef __cplusplus
extern "C"
{
#endif

/** \file F_SHAN.h
    \brief Wrapper functions for the SHAN library, mostly targeted at fortran applications.
*/


/** gaspi init, sets GASPI topology to NONE
 *     
 */
void f_shan_init_gaspi(void);


/** wrapper function for shan_set_data_layout
 *     
 *
 * @param segment_id       - segment handle (data)
 * @param d1               - primary data array dimension 
 * @param d2               - data array dimension
 * @param type_layout      - AoS = 0-4 (char = 0,int = 1,long = 2,float= 3,double = 4)
 *                           SoA = 5-9 (char = 5,int = 6,long = 7,float= 8,double = 9), default = 0
 *
 */
void f_shan_set_data_layout(int const segment_id
			    , long const d1
			    , long const d2
			    , int type_layout
    );
    
/** wrapper function for shan_alloc_shared
 *     
 * Note: Memory will be page-aligned.
 *
 * @param segment_id   - segment handle (data)
 * @param dataSz       - segment size in byte
 * @param shm_ptr      - shared mem pointer for allocated memory
 *
 */
void f_shan_alloc_shared(const int segment_id
			, const long dataSz
			, void **restrict shm_ptr
    );
    
/** wrapper function for shan_get_segment_id
 *  
 * returns segment_id of shared mem ptr
 *   
 * @param shm_ptr      - shared mem pointer for allocated memory
 *
 * @return segment_id  - segment handle (data)      
 */
int f_shan_get_segment_id(void *shm_ptr);


/** wrapper function for shan_alloc_get_default_mem_sz
 *     
 * @return default (total) mem sz for SHAN
 */
long f_shan_alloc_get_default_mem_sz(void);


/** wrapper function for shan_free_shared
 *     
 * @param segment_id    - segment handle (data)
 *
 */
void f_shan_free_shared(const int segment_id);


/** wrapper function for shan_free_comm
 *     
 * @param neighbor_hood_id - general neighborhood handle
 *
 */
void f_shan_free_comm(const int neighbor_hood_id);


/** wrapper function for shan_init_comm
 *     
 * @param neighbor_hood_id - general neighborhood handle
 * @param neighbors       - comm partners (neighbors)
 * @param num_neighbors   - num comm partners (neighbors)
 * @param max_nelem_send  - max number of send elements for every comm type
 * @param max_nelem_recv  - max number of recv elements for every comm type
 * @param num_type        - number of types
 * @param maxSendSz       - max send size for every comm type (byte)
 * @param maxRecvSz       - max recv size for every comm type (byte)
 * @param num_buffer      - num comm buffer
 *
 */
void f_shan_init_comm(const int neighbor_hood_id
		      , void *neighbors
		      , int num_neighbors
		      , void *max_nelem_send
		      , void *max_nelem_recv
		      , int num_type
		      , void *maxSendSz
		      , void *maxRecvSz
		      , int num_buffer
    );
    
/** wrapper function for shan_type_layout
 *     
 * @param neighbor_hood_id - general neighborhood handle
 * @param type_id        - used type segment
 * @param nelem_send     - ptr for current number of elements per send. (in shared mem, visible node locally)
 * @param nelem_recv     - ptr for current number of elements per recv. (in shared mem, visible node locally)
 * @param nelem_per_send - pointer to number of data types per send element in shared mem
 * @param nelem_per_recv - pointer to number of data types per recv element in shared mem
 * @param send_idx       - ptr for current send offset list. (in shared mem, visible node locally)
 * @param recv_idx       - ptr for current recv offset list. (in shared mem, visible node locally)
 *
 */
void f_shan_type_layout(const int neighbor_hood_id
			, const int type_id
			, void **nelem_send
			, void **nelem_recv
			, void **nelem_per_send 
			, void **nelem_per_recv
			, void **send_idx
			, void **recv_idx
    );


/** wrapper function for shan_comm_wait4All
 *     
 * @param neighbor_hood_id - general neighborhood handle
 * @param f_segment_list   - list of (data) segment_ids
 * @param f_type_list      - list of types
 * @param num_comm         - number of neighborhood exchanges
 *
 */
void f_shan_comm_wait4All(const int neighbor_hood_id  
			   , void *f_segment_list
			   , void *f_type_list
			   , int num_comm
    );

/** wrapper function for shan_comm_wait4AllRecv
 *     
 * @param neighbor_hood_id - general neighborhood handle
 * @param segment_id     - (data) segment handle
 * @param type_id        - used type id
 * @param buffer_id      - used comm buffer id
 *
 */
void f_shan_comm_wait4Buffer(const int neighbor_hood_id  
			     , const int segment_id
			     , const int type_id
			     , const int buffer_id
    );


/** wrapper function for shan_comm_local_rank
 *  
 * @param neighbor_hood_id - general neighborhood handle
 * @param idx              - comm index for target rank in neighborhood
 *
 */
int f_shan_comm_local_rank(const int neighbor_hood_id
			    , int idx
    );

/** wrapper function for shan_comm_notify_or_write
 *  
 * @param neighbor_hood_id - general neighborhood handle
 * @param segment_id     - data segment handle
 * @param type_id        - used type id
 * @param buffer_id      - used comm buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 */
void f_shan_comm_notify_or_write(const int neighbor_hood_id
				 , const int segment_id
				 , const int type_id
				 , const int buffer_id
				 , int idx
    );
    

/** wrapper function for shan_comm_notify
 *  
 * @param neighbor_hood_id - general neighborhood handle
 * @param segment_id     - data segment handle
 * @param type_id        - used type id
 * @param buffer_id        - used comm buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 */
void f_shan_comm_notify(const int neighbor_hood_id
			, const int segment_id
			, const int type_id
			, const int buffer_id
			, int idx
    );
    

/** wrapper function for shan_comm_write
 *  
 * @param neighbor_hood_id - general neighborhood handle
 * @param segment_id     - data segment handle
 * @param type_id        - used type id
 * @param buffer_id        - used comm buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 */
void f_shan_comm_write(const int neighbor_hood_id
		       , const int segment_id
		       , const int type_id
		       , const int buffer_id
		       , int idx
    );
    


/** wrapper function for shan_comm_post_recv
 *  
 * @param neighbor_hood_id - general neighborhood handle
 * @param segment_id     - data segment handle
 * @param type_id        - used type id
 * @param buffer_id        - used comm buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 */
void f_shan_comm_post_recv(const int neighbor_hood_id
			   , const int segment_id
			   , const int type_id
			   , const int buffer_id
			   , int idx
    );
    

/** wrapper function for shan_track_ptr_name
 *  
 * @param ptr - pointer
 */
void f_shan_track_ptr_name(void **ptr);
    
#ifdef __cplusplus
}
#endif

#endif
