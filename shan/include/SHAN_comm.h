/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef SHAN_COMM_H
#define SHAN_COMM_H


#include <stdio.h>
#include <stdlib.h>

#include "SHAN_segment.h"

#ifdef __cplusplus
extern "C"
{
#endif

/** \file SHAN_comm.h
 *  \brief SHAN_comm header for persistant communication in shared memory.
 *   
 */

#define MAX_NEIGHBOR_HOOD 32      //!< max number of neighborhoods


/** Type struct, visible in shared memory
 */
typedef struct
{
    int *nelem_send;             //!< current number of send elements per neighbor
    int *nelem_recv;             //!< current num recv elements per neighbor
    int *nelem_per_send;         //!< number of integral types per send 
    int *nelem_per_recv;         //!< number of integral types per recv 
    long *send_offset_index;     //!< list of send offset indices per neighbor
    long *recv_offset_index;     //!< list of recv offset indices per neighbor
} type_local_t;


/** Segment struct, rank_local, holds all segment information.
 */
typedef struct
{
    int shan_id;                //!< shared segment id
    long dataSz;                //!< segment size array
    void *shan_ptr;             //!< local segment pointer    
} shan_remote_t;


/** comm struct, holds all communication information per buffer id.
 */
typedef struct
{
    long maxSendSz;              //!< max send size per buffer (byte)
    long maxRecvSz;              //!< max recv size per buffer (byte)
    long SendOffset[2];          //!< local offset for send per buffer id(byte)
    long RecvOffset[2];          //!< local offset for recv per buffer id(byte)
    int *local_send_count;       //!< send stage counter array, per buffer id
    unsigned char *local_stage_count; //!< generic stage counter for wait4All(Send/Recv)

#ifdef USE_REMOTE_COMM_MPI
    MPI_Request *send_req;
    MPI_Request *recv_req;
#endif


} shan_buffer_t;

    
/** comm struct, holds all communication information per type.
 */
typedef struct
{
    long elemOffset;             //!< element offset in shared mem
    int  max_nelem_send;         //!< max nelem send per type (byte)
    int  max_nelem_recv;         //!< max nelem recv per type (byte)
} shan_element_t;


/** neighborhood comm struct, holds all communication 
 *  information for the neighborhood.
 */
typedef struct
{
    int neighbor_hood_id;       //!< neighborhood id
    MPI_Comm MPI_COMM_SHM;      //!< shared MPI communicator
    MPI_Comm MPI_COMM_ALL;      //!< global MPI communicator

    int num_neighbors;          //!< num comm partners (neighbors)
    int num_local;              //!< node local number of comm partners
    int *neighbors;             //!< list of neighbors, per rank
    int *RemoteCommIndex;       //!< the remote index corresponding to own rank
    int *RemoteNumNeighbors;    //!< remote number of neighbors for RemoteCommIndex
    int max_num_neighbors;      //!< global max of num_neighbors

    int num_buffer;             //!< num comm buffers
    int num_type;               //!< num types
    long *typeOffset;           //!< type offsets for all node local ranks

    shan_segment_t shared_segment; //!< shared window for local communication
    shan_element_t *type_element;  //!< local comm data for local communication.
    shan_buffer_t *comm_buffer; //!< comm buffer for remote communication.

    long remoteSz;              //!< remote comm size, all types, send + recv (byte)
    shan_remote_t remote_segment;  //!< private segment for remote communication  

    int nProcLocal;             //!< num local ranks in shared mem
    int iProcLocal;             //!< local rank id
    int nProcGlobal;            //!< num global ranks
    int iProcGlobal;            //!< global rank id

    int master;                 //!< master of shared segment (local rank 0)
    int *localRank;             //!< local rank of comm index
    int *remote_master;         //!< global list of masters

} shan_neighborhood_t;


/** Initialize persistant communication for shared mem and GASPI.
 *  requires bidirectional communication for synchronization in
 *  one-sided communication.
 * 
 *  A zero length messages will work, no message at all will fail.
 * 
 *  - allocates shared and private mem for communication (double buffered).
 *  - figures out local and remote comm partners.
 *  - negotiates remote number f neighbors and comm index
 *
 * @param neighborhood_id - general neighborhood handle
 * @param neighbor_hood_id - neighborhood id
 * @param neighbors     - comm partners (neighbors)
 * @param num_neighbors - num comm partners (neighbors)
 * @param max_nelem_send - max number of send elements per type
 * @param max_nelem_recv - max number of recv elements per type
 * @param num_type      - number of types
 * @param maxSendSz     - max send size for every comm buffer
 * @param maxRecvSz     - max recv size for every comm buffer
 * @param num_buffer    - number of independent comm buffers/channels
 * @param MPI_COMM_SHM  - MPI shared mem communicator
 * @param MPI_COMM_ALL  - embedding of shared communicator (typically MPI_COMM_WORLD) 
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_init_comm(shan_neighborhood_t *const neighborhood_id
			, int neighbor_hood_id
			, int *neighbors
			, int num_neighbors
			, int *max_nelem_send
			, int *max_nelem_recv
			, int num_type 
			, long *maxSendSz
			, long *maxRecvSz
			, int num_buffer
			, MPI_Comm MPI_COMM_SHM
			, MPI_Comm MPI_COMM_ALL
    );
    
/** Free communication ressources
 *
 * @param neighborhood_id - general neighborhood handle
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_free_comm(shan_neighborhood_t *const neighborhood_id);


/** Writes data or flags data as readable.
 *  
 *  - aggregates send data into linear buffer or
 *  - flags data as readable 
 *
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_notify_or_write(shan_neighborhood_t *const neighborhood_id
			      , shan_segment_t *data_segment
			      , int type_id
			      , int buffer_id
			      , int idx
    );


/** Flags node local data as readable.
 *  
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_notify(shan_neighborhood_t *const neighborhood_id
		     , shan_segment_t *data_segment
		     , int type_id
		     , int buffer_id
		     , int idx
    );


/** Writes data to remote nodes
 *  
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_write(shan_neighborhood_t *const neighborhood_id
		    , shan_segment_t *data_segment
		    , int type_id
		    , int buffer_id
		    , int idx
    );

    
/** Post recv for MPI version (remote comm)
 *  
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_post_recv(shan_neighborhood_t *const neighborhood_id
			, shan_segment_t *const data_segment
			, int type_id
			, int buffer_id
			, int idx
    );


/** Testing for recv request, MPI version (remote comm)
 *  
 * @param neighborhood_id - general neighborhood handle
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
void shan_comm_test4_request(shan_neighborhood_t *const neighborhood_id
			     , int buffer_id
			     , int idx
    );

/** Waits for multiple buffers on entire neighborhood
 *  
 *  - waits for either shared memory notifications
 *    or remote GASPI notifications 
 *  - directly converts send type into recv type in shared memory,
 *    dynamically load balanced with conversion either at src or target rank.
 *
 *  - unpacks pipelined remote communictation
 *    into the current receive type.
 *
 *
 * @param neighborhood_id - general neighborhood handle
 * @param segments     - list of data segment handles
 * @param types        - list of types
 * @param num_buffer   - num independent communication channels
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */

int shan_comm_wait4All(shan_neighborhood_t *const neighborhood_id  
		       , shan_segment_t *const *segments
		       , int *types
		       , int num_buffer
    );


/** Waits for entire neighborhood, single buffer
 *  
 *  (see shan_comm_wait4All, single buffer) 
 *
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_wait4Buffer(shan_neighborhood_t *const neighborhood_id  
			  , shan_segment_t *data_segment
			  , int type_id
			  , int buffer_id
    );


/** Waits for entire neighborhood, all receives
 *  
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_wait4AllRecv(shan_neighborhood_t *const neighborhood_id  
			   , shan_segment_t *data_segment
			   , int type_id
			   , int buffer_id
    );

/** waits for specific receive requests.
 *
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
 int shan_comm_wait4Recv(shan_neighborhood_t *const neighborhood_id
			 , shan_segment_t *data_segment
			 , int type_id
			 , int buffer_id
			 , int idx
     );


/** Tests for specific receive requests.
 *
 * @param neighborhood_id - general neighborhood handle
 * @param data_segment   - data segment handle
 * @param type_id        - type index
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_test4Recv(shan_neighborhood_t *const neighborhood_id
			, shan_segment_t *data_segment
			, int type_id
			, int buffer_id
			, int idx
    );


/** Waits for entire neighborhood, all sends
 *  
 * @param neighborhood_id - general neighborhood handle
 * @param buffer_id      - buffer id
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_wait4AllSend(shan_neighborhood_t *const neighborhood_id
			    , int buffer_id
    ); 

/** Waits for specific send requests.
 *
 * @param neighborhood_id - general neighborhood handle
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_wait4Send(shan_neighborhood_t *const neighborhood_id
			, int buffer_id
			, int idx
    ); 

/** Tests for specific send requests.
 *
 * @param neighborhood_id - general neighborhood handle
 * @param buffer_id      - buffer id
 * @param idx            - comm index for target rank in neighborhood
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_test4Send(shan_neighborhood_t *const neighborhood_id
			, int buffer_id
			, int idx
    ) ;

/** Gets node local rank id.
 *  
 * @param neighborhood_id - handle for neighborhood
 * @param idx             - comm idx
 *
 * @return SHAN_COMM_SUCCESS in case of success, SHAN_COMM_ERROR in case of error.
 */
int shan_comm_local_rank(shan_neighborhood_t * const neighborhood_id
			 , const int idx
    );

/** Shared mem barrier
 *  
 * @param neighborhood_id - general neighborhood handle
 *
 */
void shan_comm_shmemBarrier(shan_neighborhood_t *const neighborhood_id);

#ifdef __cplusplus
}
#endif

#endif


