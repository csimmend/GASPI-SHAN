/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#include <mpi.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/ipc.h>

#include <math.h>
#include <string.h>
#include <GASPI.h>

#include "SHAN_segment.h"
#include "shan_util.h"
#include "shan_copy.h"
#include "assert.h"


/** default comm ressource percentage 
 *  re-set via env variable GASPI_SHAN_MEM_SHARE
 */

#define MEM_SHARE_DEFAULT 0.05 

#define MAX_PTR_VAL 16384
static int shan_track_var[MAX_PTR_VAL];
static uintptr_t shan_shm_ptr[MAX_PTR_VAL];
static int shan_num_ptr_val = 0;

static int shan_alloc_shared_mpi(shan_segment_t *const segment
				 , const MPI_Comm MPI_COMM_SHM 
				 , int page_align
    )
{
    const int shan_id = segment->shan_id;
    const int shan_type = segment->shan_type;
    int i, j, k;

    int iProcLocal, nProcLocal;
    MPI_Comm_rank(MPI_COMM_SHM, &iProcLocal);
    MPI_Comm_size(MPI_COMM_SHM, &nProcLocal);

    for (i = 0; i < nProcLocal; ++i)
    {
	segment->localDataSz[i] 
	    = UP(segment->localDataSz[i], page_align); 
    }
    segment->dataSz = 0;
    for (i = 0; i < nProcLocal; ++i)
    {
	segment->ptr_array[i] = NULL;
	segment->dataSz += segment->localDataSz[i];
    }
  
    void *ptr = NULL;  
    int dsp;  
    MPI_Aint rsz;
    MPI_Info win_info;
    MPI_Info_create(&win_info);
    MPI_Info_set(win_info, "alloc_shared_noncontig", "true");
    MPI_Win_allocate_shared(segment->localDataSz[iProcLocal]
			    , page_align
			    , win_info
			    , segment->MPI_COMM_SHM
			    , &ptr
			    , &(segment->segment_win)
	);

    for (i = 0; i < nProcLocal; ++i)
    {      
	MPI_Win_shared_query (segment->segment_win 
			      , i
			      , &rsz
			      , &dsp
			      , &ptr
	    );
	segment->ptr_array[i] = (void *restrict) ((char *) ptr 
						  + sizeof(shan_layout_t)); 
	ASSERT(rsz == segment->localDataSz[i]);
    }
    segment->shan_ptr = segment->ptr_array[iProcLocal];

    return SHAN_SUCCESS; 
}


long shan_alloc_get_default_mem_sz(const MPI_Comm MPI_COMM_SHM)
{
    char *shan_mem_share = getenv("SHAN_MEM_SHARE"); 
    const double mem_share = (shan_mem_share != NULL)  
	? atof(shan_mem_share) : MEM_SHARE_DEFAULT;	

    long const pages = sysconf(_SC_AVPHYS_PAGES);
    long const page_size = sysconf (_SC_PAGESIZE);

    int nProcLocal;
    MPI_Comm_size(MPI_COMM_SHM, &nProcLocal);

    return (pages * page_size) * mem_share / nProcLocal; 
}


int shan_alloc_shared(shan_segment_t *const segment
                      , const int shan_id
                      , const int shan_type
                      , const long dataSz
                      , const MPI_Comm MPI_COMM_SHM 
    )
{
    int i;
    ASSERT(segment != NULL);
    ASSERT(dataSz >= 0);

#ifdef TRACK_SEGMENT_PTR
    ASSERT(shan_num_ptr_val < MAX_PTR_VAL);
#endif

    int initialized = 0;
    MPI_Initialized(&initialized);
    ASSERT(initialized != 0);

    int iProcLocal, nProcLocal;
    MPI_Comm_rank(MPI_COMM_SHM, &iProcLocal);
    MPI_Comm_size(MPI_COMM_SHM, &nProcLocal);

    segment->shan_id      = shan_id;
    segment->shan_type    = shan_type;

    segment->ptr_array    = check_malloc (nProcLocal * sizeof(void*));
    segment->localDataSz  = check_malloc (nProcLocal * sizeof(long));
    segment->MPI_COMM_SHM = MPI_COMM_SHM;

    long sz = dataSz + sizeof(shan_layout_t);
    MPI_Allgather(&sz
		  , 1
		  , MPI_LONG
		  , segment->localDataSz
		  , 1
		  , MPI_LONG
		  , MPI_COMM_SHM
	);

    int page_align = sysconf (_SC_PAGESIZE); 	    
    int res = shan_alloc_shared_mpi(segment
				    , MPI_COMM_SHM
				    , page_align
	);
    ASSERT(res == SHAN_SUCCESS);

    /*
     * segment layout - defaults to char (type_layout = 0)
     */
    shan_layout_t *shan_stat  
	= (shan_layout_t*) ((char*) segment->ptr_array[iProcLocal] 
			    - sizeof(shan_layout_t));
    shan_stat->localSz     = segment->localDataSz[iProcLocal];
    shan_stat->segment_id  = shan_id;
    shan_stat->d1          = 1;
    shan_stat->d2          = 1;
    shan_stat->type_layout = 0;

    /*
     * numa - first touch policy
     */
    __sync_synchronize();
    MPI_Barrier(MPI_COMM_SHM);

    ASSERT(segment->ptr_array[iProcLocal] != NULL);
    memset(segment->ptr_array[iProcLocal]
	   , 0
	   , dataSz
	);

#ifdef TRACK_SEGMENT_PTR
    for (i = 0; i < nProcLocal; ++i)
    {      
	shan_shm_ptr[shan_num_ptr_val++] 
	    = (uintptr_t) segment->ptr_array[i];
    }
#endif

    __sync_synchronize();
    MPI_Barrier(MPI_COMM_SHM);

    return SHAN_SUCCESS; 

}


void shan_track_malloc(char* var_name
		       , long bytes
		       , void **ptr
    )
{
    void *tmp = check_malloc(bytes + sizeof(shan_track_t));
    shan_track_t *shan_stat  = (shan_track_t*) tmp;
    strcpy(shan_stat->var_name, var_name);  
    void *p = (char*) tmp + sizeof(shan_track_t);

    shan_track_var[shan_num_ptr_val] = 0;
    shan_shm_ptr[shan_num_ptr_val] = (uintptr_t) p;

    int iProc;
    MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
    char log[80];
    sprintf(log, "./shan_malloc_var.rank.%d",iProc); 
    
    FILE *file;
    if ((file = fopen(log, "a+")) == NULL)
    {
	perror("-- fopen failure");
	exit(1);
    }

    fprintf(file, "# shan_malloc_name - num_segments: %d var name: %s\n"
	    ,shan_num_ptr_val, var_name);
    fclose(file);
    
    *ptr = p;
    shan_num_ptr_val++;
}

void shan_track_ptr_name(void *ptr)
{   
    ASSERT(ptr != NULL);

    int iProc;
    MPI_Comm_rank(MPI_COMM_WORLD, &iProc);

    int i, ok = -1;
    for (i = 0; i < shan_num_ptr_val; ++i)
    {
	if ((uintptr_t) ptr == shan_shm_ptr[i])
	{
	    ok = i;
	    break;
	}
    }
    ASSERT (ok != -1);
    
    if (!shan_track_var[ok])
    {
	shan_track_t *shan_stat  
	    = (shan_track_t*) ((char*) ptr - sizeof(shan_track_t));
	ASSERT(shan_stat != NULL);

	int iProc;
 	MPI_Comm_rank(MPI_COMM_WORLD, &iProc);

	char log[80];
	sprintf(log, "./shan_track_var.rank.%d",iProc); 

	FILE *file;
	if ((file = fopen(log, "a+")) == NULL)
	{
	    perror("-- fopen failure");
	    exit(1);
	}

	fprintf(file, "# shan_track_ptr_name - num_segments: %d var name: %s\n"
		,shan_num_ptr_val, shan_stat->var_name);
	fclose(file);

	shan_track_var[ok] = 1;
    }
}


void test_shan_shm_ptr(void *ptr)
{
    int i, ok = 1;
    for (i = 0; i < shan_num_ptr_val; ++i)
    {
	if ((uintptr_t) ptr == shan_shm_ptr[i])
	{
	    ok = i;
	    break;
	}
    }
    ASSERT(ok != -1);
}


int shan_get_segment_id(void *ptr)
{   
    ASSERT(ptr != NULL);
#ifdef TRACK_SEGMENT_PTR
    test_shan_shm_ptr(ptr);
#endif
    shan_layout_t *shan_stat  
	= (shan_layout_t*) ((char*) ptr - sizeof(shan_layout_t));
    ASSERT(shan_stat != NULL);
    return shan_stat->segment_id;
}


void shan_set_type_layout(void *ptr
			  , int layout
    )
{    
    ASSERT(ptr != NULL);
#ifdef TRACK_SEGMENT_PTR
    test_shan_shm_ptr(ptr);
#endif
    shan_layout_t *shan_stat  
	= (shan_layout_t*) ((char*) ptr - sizeof(shan_layout_t));
    ASSERT(shan_stat != NULL);
    shan_stat->type_layout = layout;
}

int shan_get_type_layout(void *ptr)
{    
    ASSERT(ptr != NULL);
#ifdef TRACK_SEGMENT_PTR
    test_shan_shm_ptr(ptr);
#endif
    shan_layout_t *shan_stat  
	= (shan_layout_t*) ((char*) ptr - sizeof(shan_layout_t));
    ASSERT(shan_stat != NULL);
    return shan_stat->type_layout;
}

void shan_get_array_dimension(void *ptr
			      , long *d1
			      , long *d2
    )
{  
    ASSERT(ptr != NULL);
#ifdef TRACK_SEGMENT_PTR
    test_shan_shm_ptr(ptr);
#endif
    shan_layout_t *shan_stat  
	= (shan_layout_t*) ((char*) ptr - sizeof(shan_layout_t));
    ASSERT(shan_stat != NULL);

    int const sz = shan_copy_get_type_sz(shan_stat->type_layout);
    ASSERT(shan_stat->d1 * shan_stat->d2 * sz <= shan_stat->localSz);
    
    *d1 = shan_stat->d1;
    *d2 = shan_stat->d2;
}


void shan_set_array_dimension(void *ptr
			      , long d1
			      , long d2
    )
{  
    ASSERT(ptr != NULL);
#ifdef TRACK_SEGMENT_PTR
    test_shan_shm_ptr(ptr);
#endif

    shan_layout_t *shan_stat  
	= (shan_layout_t*) ((char*) ptr - sizeof(shan_layout_t));
    ASSERT(shan_stat != NULL);

    int const sz = shan_copy_get_type_sz(shan_stat->type_layout);
    ASSERT(shan_stat->d1 * shan_stat->d2 * sz <= shan_stat->localSz);

    shan_stat->d1 = d1;
    shan_stat->d2 = d2;
}

int shan_get_shared_ptr(shan_segment_t * const segment
			, const int local
			, void **shm_ptr			  
			)
{
    *shm_ptr = segment->ptr_array[local];  
    return SHAN_SUCCESS;
}


int shan_free_shared(shan_segment_t * const segment)
{

    MPI_Barrier(segment->MPI_COMM_SHM);
    MPI_Win_free(&segment->segment_win);

    check_free(segment->localDataSz);
    check_free(segment->ptr_array);
    MPI_Barrier(segment->MPI_COMM_SHM);  
  
    return SHAN_SUCCESS; 
}






