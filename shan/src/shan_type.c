/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#include <mpi.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#ifndef USE_REMOTE_COMM_MPI
#include <GASPI.h>
#endif

#include "SHAN_segment.h"
#include "SHAN_comm.h"
#include "SHAN_type.h"

#ifndef USE_REMOTE_COMM_MPI
#include "gaspi_util.h"
#endif

#include "shan_util.h"
#include "shan_copy.h"
#include "shan_core.h"
#include "assert.h"

void shan_type_validate(shan_neighborhood_t * const neighborhood_id
			, int type_id
    )
{
    int i;
    int const iProcLocal = neighborhood_id->iProcLocal;
    int const num_neighbors = neighborhood_id->num_neighbors;
    type_local_t type_info;
    shan_get_shared_type(&type_info
			 , neighborhood_id
			 , iProcLocal
			 , num_neighbors
			 , type_id
	);
  
    /* 
     * remote num_neighbors
     */	  
    int *nngb = check_malloc (neighborhood_id->nProcGlobal * sizeof(int));  
    MPI_Allgather(&(neighborhood_id->num_neighbors)
		  , 1
		  , MPI_INT
		  , nngb
		  , 1
		  , MPI_INT
		  , neighborhood_id->MPI_COMM_ALL
	);  

    /* 
     * displacements
     */	  
    int *disp = check_malloc (neighborhood_id->nProcGlobal * sizeof(int));
    int ncomm = 0;
    for (i = 0; i < neighborhood_id->nProcGlobal; ++i)
    {
	disp[i] = ncomm;
	ncomm += nngb[i];
    }	  

    /* 
     * validate nelem send/recv
     */	  
    int *buff = check_malloc (ncomm * sizeof(int));  
    for (i = 0; i < ncomm; ++i)
    {
	buff[i] = -1;
    }
    MPI_Allgatherv(type_info.nelem_send
		   , neighborhood_id->num_neighbors
		   , MPI_INT
		   , buff
		   , nngb
		   , disp
		   , MPI_INT
		   , neighborhood_id->MPI_COMM_ALL
	);  
    for (i = 0; i < ncomm; ++i)
    {
	ASSERT(buff[i] != -1);
    }

    for (i = 0; i < num_neighbors; ++i)
    {
	int const rank = neighborhood_id->neighbors[i];
	int idx = neighborhood_id->RemoteCommIndex[i];
	ASSERT(buff[disp[rank]+idx] == type_info.nelem_recv[i]);
    }

    /* 
     * validate nelem per send/recv
     */	  
    for (i = 0; i < ncomm; ++i)
    {
	buff[i] = -1;
    }
    MPI_Allgatherv(type_info.nelem_per_send
		   , neighborhood_id->num_neighbors
		   , MPI_INT
		   , buff
		   , nngb
		   , disp
		   , MPI_INT
		   , neighborhood_id->MPI_COMM_ALL
	);  
    for (i = 0; i < ncomm; ++i)
    {
	ASSERT(buff[i] != -1);
    }

    for (i = 0; i < num_neighbors; ++i)
    {
	int const rank = neighborhood_id->neighbors[i];
	int idx = neighborhood_id->RemoteCommIndex[i];
	ASSERT(buff[disp[rank]+idx] == type_info.nelem_per_recv[i]);
    }
	    
    check_free(nngb);
    check_free(disp);
    check_free(buff);

}


int shan_get_shared_type(type_local_t *type_info
			 , shan_neighborhood_t *neighborhood_id
			 , int local_rank
			 , int num_neighbors
			 , int type_id
			 )
{  
  shan_segment_t *shared_segment = &(neighborhood_id->shared_segment);
  void *shm_ptr = shared_segment->ptr_array[local_rank];  
  shan_comm_get_type(type_info
		     , shm_ptr
		     , num_neighbors
		     , &(neighborhood_id->type_element[type_id])
		     ); 
  
  return SHAN_SUCCESS;
}


void shan_set_data_layout(shan_segment_t *data_segment
			  , long const d1
			  , long const d2
			  , int const type_layout
    )
{
    int iProcLocal;
    MPI_Comm_rank(data_segment->MPI_COMM_SHM, &iProcLocal);

    void *shm_ptr = data_segment->ptr_array[iProcLocal];  
    shan_set_type_layout(shm_ptr, type_layout);
    shan_set_array_dimension(shm_ptr
			      , d1
			      , d2
	);
}


int shan_comm_get_type(type_local_t *type_info
		       , void *shm_ptr
		       , int num_neighbors
		       , shan_element_t *type_element
		       )
{
    long typeOffset          = num_neighbors * type_element->elemOffset;
    int max_nelem_send       = type_element->max_nelem_send;
    int max_nelem_recv       = type_element->max_nelem_recv;

    long const maxSz = 4 * num_neighbors * sizeof(int)
	+ num_neighbors * (max_nelem_send + max_nelem_recv) * sizeof(long);
  
    type_info->nelem_send    = (int*) ((char*) shm_ptr + typeOffset);
    typeOffset              += num_neighbors * sizeof(int);
    type_info->nelem_recv    = (int*) ((char*) shm_ptr + typeOffset);
    typeOffset              += num_neighbors * sizeof(int);
    type_info->nelem_per_send       = (int*) ((char*) shm_ptr + typeOffset);
    typeOffset              += num_neighbors * sizeof(int);
    type_info->nelem_per_recv       = (int*) ((char*) shm_ptr + typeOffset);
    typeOffset              += num_neighbors * sizeof(int);
    type_info->send_offset_index = (long*) ((char*) shm_ptr + typeOffset);
    typeOffset              += max_nelem_send * num_neighbors * sizeof(long);
    type_info->recv_offset_index = (long*) ((char*) shm_ptr + typeOffset);
    typeOffset              += max_nelem_recv * num_neighbors * sizeof(long);

    ASSERT(typeOffset == num_neighbors * type_element->elemOffset + maxSz);

    return SHAN_SUCCESS;
  
}

int shan_type_layout(shan_neighborhood_t *neighborhood_id
		     , int type_id
		     , int **nelem_send
		     , int **nelem_recv
		     , int **nelem_per_send
		     , int **nelem_per_recv
		     , long **send_idx
		     , long **recv_idx
    )
{
    int const iProcLocal = neighborhood_id->iProcLocal;
    int const num_neighbors = neighborhood_id->num_neighbors;
    type_local_t type_info;
    shan_get_shared_type(&type_info
			 , neighborhood_id
			 , iProcLocal
			 , num_neighbors
			 , type_id
	);
            
    *nelem_send      = type_info.nelem_send;
    *nelem_recv      = type_info.nelem_recv;
    *nelem_per_send  = type_info.nelem_per_send;
    *nelem_per_recv  = type_info.nelem_per_recv;
    *send_idx        = type_info.send_offset_index;
    *recv_idx        = type_info.recv_offset_index;
        
    return SHAN_SUCCESS;

}


int shan_type_free(shan_segment_t *type_segment)
{
    int res = shan_free_shared(type_segment);
    ASSERT(res == SHAN_SUCCESS);
    
    return SHAN_SUCCESS;
}


void shan_comm_write_local(shan_neighborhood_t *neighborhood_id
			 , shan_segment_t *data_segment
			 , int const type_id
			 , int const idx
			 )
{
    int const iProcLocal = neighborhood_id->iProcLocal;
    int iProcRemote, i, j;

    int const rank = neighborhood_id->neighbors[idx];
    ASSERT ((iProcRemote = neighborhood_id->localRank[idx]) != -1);  
  
    int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[idx];
    int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[idx];

    type_local_t type_info_src;
    shan_get_shared_type(&type_info_src
			 , neighborhood_id
			 , iProcLocal
			 , neighborhood_id->num_neighbors
			 , type_id
	);
  
    int  src_nelem_send     = type_info_src.nelem_send[idx];
    int  src_nelem_per_send = type_info_src.nelem_per_send[idx];
    long *src_send_index    = type_info_src.send_offset_index
	+ idx * neighborhood_id->type_element[type_id].max_nelem_send;		      

    type_local_t type_info_dest;
    shan_get_shared_type(&type_info_dest
			 , neighborhood_id
			 , iProcRemote
			 , RemoteNumNeighbors
			 , type_id
	);

    int  dest_nelem_per_recv = type_info_dest.nelem_per_recv[RemoteCommIdx];
    long *dest_recv_index    = type_info_dest.recv_offset_index
	+ RemoteCommIdx * neighborhood_id->type_element[type_id].max_nelem_recv;    
    ASSERT(src_nelem_per_send == dest_nelem_per_recv);

    void *send_ptr = data_segment->ptr_array[iProcLocal];  
    void *recv_ptr = data_segment->ptr_array[iProcRemote];  

    int layout_send = shan_get_type_layout(send_ptr);
    int layout_recv = shan_get_type_layout(recv_ptr);
    ASSERT(layout_send == layout_recv);

    long d1_send, d2_send;
    shan_get_array_dimension(send_ptr 
			     , &d1_send
			     , &d2_send);
    long d1_recv, d2_recv;
    shan_get_array_dimension(recv_ptr 
			     , &d1_recv
			     , &d2_recv);

    ASSERT(d1_send == d1_recv);
    if (layout_send / MAX_LAYOUTS == 0)
    {    
	shan_copy_src_dest_AoS( src_nelem_send
				, src_nelem_per_send
				, src_send_index 
				, send_ptr
				, dest_recv_index 
				, recv_ptr
				, d1_send
				, layout_send
	);
    }
    else if (layout_send / MAX_LAYOUTS == 1)
    {    
	shan_copy_src_dest_SoA( src_nelem_send
				, src_nelem_per_send
				, src_send_index 
				, send_ptr
				, dest_recv_index 
				, recv_ptr
				, d1_send
				, d2_recv
				, d2_send
				, layout_send
	    );	
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
	
    }

    ASSERT(type_info_dest.nelem_recv[RemoteCommIdx] == src_nelem_send);
    ASSERT(type_info_dest.nelem_per_recv[RemoteCommIdx] == src_nelem_per_send);

}

void shan_comm_get_local(shan_neighborhood_t *neighborhood_id
			 , shan_segment_t *data_segment
			 , int const type_id
			 , int const idx
			 )
{
    int const iProcLocal = neighborhood_id->iProcLocal;
    int iProcRemote, i, j;

    int const rank = neighborhood_id->neighbors[idx];
    ASSERT ((iProcRemote = neighborhood_id->localRank[idx]) != -1);  
  
    int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[idx];
    int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[idx];

    type_local_t type_info_src;
    shan_get_shared_type(&type_info_src
			 , neighborhood_id
			 , iProcRemote
			 , RemoteNumNeighbors
			 , type_id
	);
  
    int  src_nelem_send     = type_info_src.nelem_send[RemoteCommIdx];
    int  src_nelem_per_send = type_info_src.nelem_per_send[RemoteCommIdx];
    long *src_send_index    = type_info_src.send_offset_index
	+ RemoteCommIdx * neighborhood_id->type_element[type_id].max_nelem_send;		      

    type_local_t type_info_dest;
    shan_get_shared_type(&type_info_dest
			 , neighborhood_id
			 , iProcLocal
			 , neighborhood_id->num_neighbors
			 , type_id
	);

    int  dest_nelem_per_recv = type_info_dest.nelem_per_recv[idx];
    long *dest_recv_index    = type_info_dest.recv_offset_index
	+ idx * neighborhood_id->type_element[type_id].max_nelem_recv;
    ASSERT(src_nelem_per_send == dest_nelem_per_recv);

    void *send_ptr = data_segment->ptr_array[iProcRemote];  
    void *recv_ptr = data_segment->ptr_array[iProcLocal];  

    int layout_send = shan_get_type_layout(send_ptr);
    int layout_recv = shan_get_type_layout(recv_ptr);
    ASSERT(layout_send == layout_recv);

    long d1_send, d2_send;
    shan_get_array_dimension(send_ptr 
			     , &d1_send
			     , &d2_send);
    long d1_recv, d2_recv;
    shan_get_array_dimension(recv_ptr 
			     , &d1_recv
			     , &d2_recv);

    ASSERT(d1_send == d1_recv);
    if (layout_send / MAX_LAYOUTS == 0)
    {    
	shan_copy_src_dest_AoS( src_nelem_send
				, src_nelem_per_send
				, src_send_index 
				, send_ptr
				, dest_recv_index 
				, recv_ptr
				, d1_send
				, layout_send
	);
    }
    else if (layout_send / MAX_LAYOUTS == 1)
    {    
	shan_copy_src_dest_SoA( src_nelem_send
				, src_nelem_per_send
				, src_send_index 
				, send_ptr
				, dest_recv_index 
				, recv_ptr
				, d1_send
				, d2_recv
				, d2_send
				, layout_send
	    );	
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
	
    }

#ifdef USE_VARIABLE_MESSAGE_LEN
    type_info_dest.nelem_recv[idx] = src_nelem_send; 
    type_info_dest.nelem_per_recv[idx] = src_nelem_per_send; 
#else
    ASSERT(type_info_dest.nelem_recv[idx] == src_nelem_send);
    ASSERT(type_info_dest.nelem_per_recv[idx] == src_nelem_per_send);
#endif  


}


void shan_comm_get_remote(shan_neighborhood_t *neighborhood_id
			  , shan_segment_t *data_segment
			  , int const type_id
			  , int const buffer_id
			  , int const idx
    )
{
    int i, j;
    int const iProcLocal    = neighborhood_id->iProcLocal;
    int const num_neighbors = neighborhood_id->num_neighbors;
  
    int const sid = (neighborhood_id->comm_buffer[buffer_id].local_send_count[idx] - 1) % 2;  
    shan_remote_t *const remote_segment = &(neighborhood_id->remote_segment);

    type_local_t type_info;
    shan_get_shared_type(&type_info
			 , neighborhood_id
			 , iProcLocal
			 , num_neighbors
			 , type_id
	);

    int const nelem_recv     = type_info.nelem_recv[idx];	  
    ASSERT(nelem_recv <= neighborhood_id->type_element[type_id].max_nelem_recv);

    int const nelem_per_recv = type_info.nelem_per_recv[idx];	  
    long *const recv_index   = type_info.recv_offset_index
	+ idx * neighborhood_id->type_element[type_id].max_nelem_recv;
    
    long buffer_offset = num_neighbors * neighborhood_id->comm_buffer[buffer_id].RecvOffset[sid]
        + idx * neighborhood_id->comm_buffer[buffer_id].maxRecvSz;    
    void *comm_ptr = (char*) remote_segment->shan_ptr + buffer_offset;    
    void *data_ptr = data_segment->ptr_array[iProcLocal];  

    int layout = shan_get_type_layout(data_ptr);
    long d1, d2;
    shan_get_array_dimension(data_ptr 
			     , &d1
			     , &d2);

    void *base_ptr = (char*) comm_ptr + NELEM_COMM_HEADER * sizeof(int);

    if (layout / MAX_LAYOUTS == 0)
    {
	shan_copy_comm_dest_AoS( base_ptr
				 , nelem_recv
				 , nelem_per_recv
				 , recv_index 
				 , data_ptr
				 , d1
				 , layout
	    );
    }
    else if (layout / MAX_LAYOUTS == 1)
    {
	shan_copy_comm_dest_SoA( base_ptr
				 , nelem_recv
				 , nelem_per_recv
				 , recv_index 
				 , data_ptr
				 , d1
				 , d2
				 , layout
	    );
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }
}

