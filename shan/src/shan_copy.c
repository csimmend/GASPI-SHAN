/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#include "SHAN_type.h"
#include "shan_copy.h"
#include "assert.h"


int shan_copy_get_type_sz(int layout)
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const type = layout % MAX_LAYOUTS;

    if (type == 0) 
    {
	return sizeof(char);
    } 
    else if (type == 1)
    {
	return sizeof(int);
    }
    else if (type == 2)
    {
	return sizeof(long);
    }
    else if (type == 3)
    {
	return sizeof(float);
    }
    else if (type == 4)
    {
	return sizeof(double);
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }
}

void shan_copy_src_dest_AoS( int  nelem_send
			     , int  nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , long *recv_index 
			     , void *recv_ptr
			     , long d1
			     , int layout
    )
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const SoA  = layout / MAX_LAYOUTS;
    int const type = layout % MAX_LAYOUTS;

    ASSERT(SoA == 0);

    if (type == 0)
    {
	for (i = 0; i < nelem_send; ++i)
	{
	    char *restrict src  = (char*) send_ptr 
		+ send_index[i] * d1;
	    char *restrict dest = (char*) recv_ptr 
		+ recv_index[i] * d1;
	    memcpy(dest, src, nelem_per_send * d1);
	}
    }
    else if (type == 1)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    int *restrict src  = (int*) send_ptr;
	    int *restrict dest = (int*) recv_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + recv_index[i]) 
		    = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		int *restrict src  = (int*) send_ptr
		    + send_index[i] * d1;
		int *restrict dest = (int*) recv_ptr 
		    + recv_index[i] * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 2)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    long *restrict src  = (long*) send_ptr;
	    long *restrict dest = (long*) recv_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + recv_index[i]) 
		    = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		long *restrict src  = (long*) send_ptr
		    + send_index[i] * d1;
		long *restrict dest = (long*) recv_ptr 
		    + recv_index[i] * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 3)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    float *restrict src  = (float*) send_ptr;
	    float *restrict dest = (float*) recv_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + recv_index[i]) 
		    = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		float *restrict src  = (float*) send_ptr 
		    + send_index[i] * d1;
		float *restrict dest = (float*) recv_ptr 
		    + recv_index[i] * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 4)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    double *restrict src  = (double*) send_ptr;
	    double *restrict dest = (double*) recv_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + recv_index[i]) 
		    = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		double *restrict src  = (double*) send_ptr 
		    + send_index[i] * d1;
		double *restrict dest = (double*) recv_ptr 
		    + recv_index[i] * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }

}


void shan_copy_src_dest_SoA( int  nelem_send
			     , int  nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , long *recv_index 
			     , void *recv_ptr
			     , long d1_send
			     , long d2_recv
			     , long d2_send
			     , int layout
    )
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const SoA  = layout / MAX_LAYOUTS;
    int const type = layout % MAX_LAYOUTS;

    ASSERT (SoA == 1); 

    if (type == 0)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		char *restrict src  = (char*) send_ptr 
		    + k * d2_send;
		char *restrict dest = (char*) recv_ptr 
		    + k * d2_recv;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + recv_index[i]) 
			= *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    char *restrict src  = (char*) send_ptr 
			+ k * d2_send + send_index[i];
		    char *restrict dest = (char*) recv_ptr 
			+ k * d2_recv + recv_index[i];
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 1)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		int *restrict src  = (int*) send_ptr 
		    + k * d2_send;
		int *restrict dest = (int*) recv_ptr 
		    + k * d2_recv;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + recv_index[i]) 
			= *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    int *restrict src  = (int*) send_ptr 
			+ k * d2_send + send_index[i];
		    int *restrict dest = (int*) recv_ptr 
			+ k * d2_recv + recv_index[i];
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 2)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		long *restrict src  = (long*) send_ptr 
		    + k * d2_send;
		long *restrict dest = (long*) recv_ptr 
		    + k * d2_recv;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + recv_index[i]) 
			= *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    long *restrict src  = (long*) send_ptr 
			+ k * d2_send + send_index[i];
		    long *restrict dest = (long*) recv_ptr 
			+ k * d2_recv + recv_index[i];
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 3)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		float *restrict src  = (float*) send_ptr 
		    + k * d2_send;
		float *restrict dest = (float*) recv_ptr 
		    + k * d2_recv;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + recv_index[i]) 
			= *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    float *restrict src  = (float*) send_ptr 
			+ k * d2_send + send_index[i];
		    float *restrict dest = (float*) recv_ptr 
			+ k * d2_recv + recv_index[i];
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 4)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		double *restrict src  = (double*) send_ptr 
		    + k * d2_send;
		double *restrict dest = (double*) recv_ptr 
		    + k * d2_recv;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + recv_index[i]) 
			= *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1_send; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    double *restrict src  = (double*) send_ptr 
			+ k * d2_send + send_index[i];
		    double *restrict dest = (double*) recv_ptr 
			+ k * d2_recv + recv_index[i];
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }


}


void shan_copy_src_comm_AoS( int nelem_send
			     , int nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , void *comm_ptr
			     , long d1
			     , int layout
    )
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const SoA  = layout / MAX_LAYOUTS;
    int const type = layout % MAX_LAYOUTS;

    ASSERT(SoA == 0);

    if (type == 0)
    {
	for (i = 0; i < nelem_send; ++i)
	{
	    char *restrict src  = (char*) send_ptr 
		+ send_index[i] * d1;
	    char *restrict dest = (char*) comm_ptr 
		+ i * nelem_per_send * d1;
	    memcpy(dest, src, nelem_per_send * d1);
	}
    }
    else if (type == 1)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    int *restrict src  = (int*) send_ptr;
	    int *restrict dest = (int*) comm_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + i) = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		int *restrict src  = (int*) send_ptr 
		    + send_index[i] * d1;
		int *restrict dest = (int*) comm_ptr 
		    + i * nelem_per_send * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 2)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    long *restrict src  = (long*) send_ptr;
	    long *restrict dest = (long*) comm_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + i) = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		long *restrict src  = (long*) send_ptr 
		    + send_index[i] * d1;
		long *restrict dest = (long*) comm_ptr 
		    + i * nelem_per_send * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 3)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    float *restrict src  = (float*) send_ptr;
	    float *restrict dest = (float*) comm_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + i) = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		float *restrict src  = (float*) send_ptr 
		    + send_index[i] * d1;
		float *restrict dest = (float*) comm_ptr 
		    + i * nelem_per_send * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 4)
    {
	if (nelem_per_send == 1 && d1 == 1)
	{
	    double *restrict src  = (double*) send_ptr;
	    double *restrict dest = (double*) comm_ptr;
	    for (i = 0; i < nelem_send; ++i)
	    {
		*(dest + i) = *(src + send_index[i]);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_send; ++i)
	    {
		double *restrict src  = (double*) send_ptr 
		    + send_index[i] * d1;
		double *restrict dest = (double*) comm_ptr 
		    + i * nelem_per_send * d1;
		for (j = 0; j < nelem_per_send * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }
 
}


void shan_copy_src_comm_SoA( int nelem_send
			     , int nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , void *comm_ptr
			     , long d1
			     , long d2
			     , int layout
    )
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const SoA  = layout / MAX_LAYOUTS;
    int const type = layout % MAX_LAYOUTS;

    ASSERT(SoA == 1);

    if (type == 0)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		char *restrict src  = (char*) send_ptr 
		    + k * d2;
		char *restrict dest = (char*) comm_ptr 
		    + k * nelem_send;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + i) = *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    char *restrict src  = (char*) send_ptr 
			+ k * d2 + send_index[i];
		    char *restrict dest = (char*) comm_ptr 
			+ k * nelem_send * nelem_per_send + i * nelem_per_send;
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 1)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		int *restrict src  = (int*) send_ptr 
		    + k * d2;
		int *restrict dest = (int*) comm_ptr 
		    + k * nelem_send;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + i) = *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    int *restrict src  = (int*) send_ptr 
			+ k * d2 + send_index[i];
		    int *restrict dest = (int*) comm_ptr 
			+ k * nelem_send * nelem_per_send + i * nelem_per_send;
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}	

    }
    else if (type == 2)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		long *restrict src  = (long*) send_ptr 
		    + k * d2;
		long *restrict dest = (long*) comm_ptr 
		    + k * nelem_send;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + i) = *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    long *restrict src  = (long*) send_ptr 
			+ k * d2 + send_index[i];
		    long *restrict dest = (long*) comm_ptr 
			+ k * nelem_send * nelem_per_send + i * nelem_per_send;
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}	

    }
    else if (type == 3)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		float *restrict src  = (float*) send_ptr 
		    + k * d2;
		float *restrict dest = (float*) comm_ptr 
		    + k * nelem_send;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + i) = *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    float *restrict src  = (float*) send_ptr 
			+ k * d2 + send_index[i];
		    float *restrict dest = (float*) comm_ptr 
			+ k * nelem_send * nelem_per_send + i * nelem_per_send;
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}	

    }
    else if (type == 4)
    {
	if (nelem_per_send == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		double *restrict src  = (double*) send_ptr 
		    + k * d2;
		double *restrict dest = (double*) comm_ptr 
		    + k * nelem_send;
		for (i = 0; i < nelem_send; ++i)
		{
		    *(dest + i) = *(src + send_index[i]);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_send; ++i)
		{
		    double *restrict src  = (double*) send_ptr 
			+ k * d2 + send_index[i];
		    double *restrict dest = (double*) comm_ptr 
			+ k * nelem_send * nelem_per_send + i * nelem_per_send;
		    for (j = 0; j < nelem_per_send; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}	
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }
 
}


void shan_copy_comm_dest_AoS( void *comm_ptr
			      , int nelem_recv
			      , int  nelem_per_recv
			      , long *recv_index 
			      , void *recv_ptr
			      , long d1
			      , int layout
    )
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const SoA  = layout / MAX_LAYOUTS;
    int const type = layout % MAX_LAYOUTS;


    ASSERT(SoA == 0); 

    if (type == 0)
    {
	for (i = 0; i < nelem_recv; ++i)
	{
	    char *restrict src  = (char*) comm_ptr + + i * nelem_per_recv * d1;
	    char *restrict dest = (char*) recv_ptr + recv_index[i] * d1;
	    memcpy(dest, src, nelem_per_recv * d1);
	}
    }
    else if (type == 1)
    {
	if (nelem_per_recv == 1 && d1 == 1)
	{
	    int *restrict src  = (int*) comm_ptr;
	    int *restrict dest = (int*) recv_ptr;
	    for (i = 0; i < nelem_recv; ++i)
	    {
		*(dest + recv_index[i]) = *(src + i);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_recv; ++i)
	    {
		int *restrict src  = (int*) comm_ptr + i * nelem_per_recv * d1;
		int *restrict dest = (int*) recv_ptr + recv_index[i] * d1;
		for (j = 0; j < nelem_per_recv * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 2)
    {
	if (nelem_per_recv == 1 && d1 == 1)
	{
	    long *restrict src  = (long*) comm_ptr;
	    long *restrict dest = (long*) recv_ptr;
	    for (i = 0; i < nelem_recv; ++i)
	    {
		*(dest + recv_index[i]) = *(src + i);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_recv; ++i)
	    {
		long *restrict src  = (long*) comm_ptr + i * nelem_per_recv * d1;
		long *restrict dest = (long*) recv_ptr + recv_index[i] * d1;
		for (j = 0; j < nelem_per_recv * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 3)
    {
	if (nelem_per_recv == 1 && d1 == 1)
	{
	    float *restrict src  = (float*) comm_ptr;
	    float *restrict dest = (float*) recv_ptr;
	    for (i = 0; i < nelem_recv; ++i)
	    {
		*(dest + recv_index[i]) = *(src + i);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_recv; ++i)
	    {
		float *restrict src  = (float*) comm_ptr + i * nelem_per_recv * d1;
		float *restrict dest = (float*) recv_ptr + recv_index[i] * d1;
		for (j = 0; j < nelem_per_recv * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else if (type == 4)
    {
	if (nelem_per_recv == 1 && d1 == 1)
	{
	    double *restrict src  = (double*) comm_ptr;
	    double *restrict dest = (double*) recv_ptr;
	    for (i = 0; i < nelem_recv; ++i)
	    {
		*(dest + recv_index[i]) = *(src + i);
	    }    
	}
	else
	{
	    for (i = 0; i < nelem_recv; ++i)
	    {
		double *restrict src  = (double*) comm_ptr + i * nelem_per_recv * d1;
		double *restrict dest = (double*) recv_ptr + recv_index[i] * d1;
		for (j = 0; j < nelem_per_recv * d1; ++j)
		{
		    *(dest + j) = *(src + j);
		}    
	    }
	}
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }

}


void shan_copy_comm_dest_SoA( void *comm_ptr
			      , int nelem_recv
			      , int  nelem_per_recv
			      , long *recv_index 
			      , void *recv_ptr
			      , long d1
			      , long d2
			      , int layout
    )
{
    int i, j, k;

    /*
     * type layouts:
     *
     * layout ==  0 - char,   AoS
     * layout ==  1 - int,    AoS
     * layout ==  2 - long,   AoS
     * layout ==  3 - float,  AoS
     * layout ==  4 - double, AoS
     *
     * layout ==  5 - char,   SoA
     * layout ==  6 - int,    SoA
     * layout ==  7 - long,   SoA
     * layout ==  8 - float,  SoA
     * layout ==  9 - double, SoA
     *
     */

    int const SoA  = layout / MAX_LAYOUTS;
    int const type = layout % MAX_LAYOUTS;

    ASSERT(SoA == 1); 

    if (type == 0)
    {
	if (nelem_per_recv == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		char *restrict src = (char*) comm_ptr 
		    + k * nelem_recv;
		char *restrict dest  = (char*) recv_ptr 
		    + k * d2;
		for (i = 0; i < nelem_recv; ++i)
		{
		    *(dest + recv_index[i]) = *(src + i);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_recv; ++i)
		{
		    char *restrict src = (char*) comm_ptr 
			+ k * nelem_recv * nelem_per_recv + i * nelem_per_recv;
		    char *restrict dest  = (char*) recv_ptr 
			+ k * d2 + recv_index[i];
		    for (j = 0; j < nelem_per_recv; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 1)
    {
	if (nelem_per_recv == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		int *restrict src = (int*) comm_ptr 
		    + k * nelem_recv;
		int *restrict dest  = (int*) recv_ptr 
		    + k * d2;
		for (i = 0; i < nelem_recv; ++i)
		{
		    *(dest + recv_index[i]) = *(src + i);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_recv; ++i)
		{
		    int *restrict src = (int*) comm_ptr 
			+ k * nelem_recv * nelem_per_recv + i * nelem_per_recv;
		    int *restrict dest  = (int*) recv_ptr 
			+ k * d2 + recv_index[i];
		    for (j = 0; j < nelem_per_recv; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 2)
    {
	if (nelem_per_recv == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		long *restrict src = (long*) comm_ptr 
		    + k * nelem_recv;
		long *restrict dest  = (long*) recv_ptr 
		    + k * d2;
		for (i = 0; i < nelem_recv; ++i)
		{
		    *(dest + recv_index[i]) = *(src + i);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_recv; ++i)
		{
		    long *restrict src = (long*) comm_ptr 
			+ k * nelem_recv * nelem_per_recv + i * nelem_per_recv;
		    long *restrict dest  = (long*) recv_ptr 
			+ k * d2 + recv_index[i];
		    for (j = 0; j < nelem_per_recv; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 3)
    {
	if (nelem_per_recv == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		float *restrict src = (float*) comm_ptr 
		    + k * nelem_recv;
		float *restrict dest  = (float*) recv_ptr 
		    + k * d2;
		for (i = 0; i < nelem_recv; ++i)
		{
		    *(dest + recv_index[i]) = *(src + i);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_recv; ++i)
		{
		    float *restrict src = (float*) comm_ptr 
			+ k * nelem_recv * nelem_per_recv + i * nelem_per_recv;
		    float *restrict dest  = (float*) recv_ptr 
			+ k * d2 + recv_index[i];
		    for (j = 0; j < nelem_per_recv; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else if (type == 4)
    {
	if (nelem_per_recv == 1)
	{
	    for (k = 0; k < d1; ++k)
	    {
		double *restrict src = (double*) comm_ptr 
		    + k * nelem_recv;
		double *restrict dest  = (double*) recv_ptr 
		    + k * d2;
		for (i = 0; i < nelem_recv; ++i)
		{
		    *(dest + recv_index[i]) = *(src + i);
		}    
	    }
	}
	else
	{
	    for (k = 0; k < d1; ++k)
	    {
		for (i = 0; i < nelem_recv; ++i)
		{
		    double *restrict src = (double*) comm_ptr 
			+ k * nelem_recv * nelem_per_recv + i * nelem_per_recv;
		    double *restrict dest  = (double*) recv_ptr 
			+ k * d2 + recv_index[i];
		    for (j = 0; j < nelem_per_recv; ++j)
		    {
			*(dest + j) = *(src + j);
		    }    
		}
	    }
	}
    }
    else
    {
	printf("Invalid layout type\n");
	exit(1);
    }


}
