/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef SHAN_CORE_H
#define SHAN_CORE_H


#include <stdio.h>
#include <stdlib.h>

#include "SHAN_segment.h"

#define NELEM_COMM_HEADER 3
#define ALIGNMENT 64


static inline
void shan_test_shared(shan_neighborhood_t *const neighborhood_id
		      , int const rank_local
		      , int const num_neighbors
		      , int const buffer_id
		      , int const idx
		      , int *rval
    )
{
    shan_segment_t *const shared_segment = &(neighborhood_id->shared_segment);
    void *const shm_ptr = shared_segment->ptr_array[rank_local];  
    long const idxOffset = num_neighbors
	* MAX_SHARED_NOTIFICATION * buffer_id + idx;
    shan_notify_test_shared((shan_notification_t *) shm_ptr
			    , idxOffset
			    , rval
	);
}

static inline
int shan_compare_swap(shan_neighborhood_t *const neighborhood_id				  
		      , int const rank_local
		      , int const num_neighbors
		      , int const buffer_id
		      , int const idx
		      , int const rval
    ) 
{
    shan_segment_t *const shared_segment = &(neighborhood_id->shared_segment);
    void *const shm_ptr = shared_segment->ptr_array[rank_local];  
    long const idxOffset = num_neighbors
	* MAX_SHARED_NOTIFICATION * buffer_id + idx;
    return shan_notify_compare_swap((shan_notification_t *) shm_ptr
				    , idxOffset
				    , rval
	);        
}


static inline
int shan_increment_local(shan_neighborhood_t *const neighborhood_id				  
			 , int const buffer_id
			 , int const idx
			 ) 
{
    shan_segment_t *const shared_segment = &(neighborhood_id->shared_segment);
    void *const shm_ptr = shared_segment->ptr_array[neighborhood_id->iProcLocal];
    long const idxOffset = neighborhood_id->num_neighbors
	* MAX_SHARED_NOTIFICATION * buffer_id + idx;
    shan_notify_increment_shared((shan_notification_t *) shm_ptr
				 , idxOffset
				 , 1
	);        
    return SHAN_SUCCESS;
}



static inline
int shan_init_local(shan_neighborhood_t *const neighborhood_id				  
			 , int const buffer_id
			 , int const idx
		         , int const val 
			 ) 
{
    shan_segment_t *const shared_segment = &(neighborhood_id->shared_segment);
    void *const shm_ptr = shared_segment->ptr_array[neighborhood_id->iProcLocal];  
    long const idxOffset = neighborhood_id->num_neighbors
	* MAX_SHARED_NOTIFICATION * buffer_id + idx;
    shan_notify_init_shared((shan_notification_t *) shm_ptr
				 , idxOffset
				 , val
	);        
    return SHAN_SUCCESS;
}


int shan_comm_write(shan_neighborhood_t *const neighborhood_id
		    , shan_segment_t *const data_segment
		    , int type_id
		    , int buffer_id
		    , int idx
    );

int shan_comm_notify(shan_neighborhood_t *const neighborhood_id
		     , shan_segment_t *const data_segment
		     , int type_id
		     , int buffer_id
		     , int idx
    );

int shan_comm_waitsome_local(shan_neighborhood_t *const neighborhood_id
			     , int const buffer_id
			     , int const idx
    );

int shan_comm_waitsome_remote(shan_neighborhood_t *const neighborhood_id
			      , int const type_id
			      , int const buffer_id
			      , int const idx
    );

#endif


