/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <mpi.h>
#include <unistd.h>
#include <stdio.h>
#include "SHAN_comm.h"

#define DATA_KEY 4710

void local_exchange_comm(int iProc
			 , int target
			 , int sz
			 , void *sbuf
			 , void *rbuf
			 , MPI_Comm MPI_COMM_ALL
    )
{
    if (target > iProc) /* first send */
    {
	MPI_Send(sbuf
		 , sz
		 , MPI_CHAR
		 , target
		 , DATA_KEY
		 , MPI_COMM_ALL
	    );
	MPI_Recv(rbuf
		 , sz
		 , MPI_CHAR
		 , target
		 , DATA_KEY
		 , MPI_COMM_ALL
		 , MPI_STATUS_IGNORE
	    );
    }
    else  /* first receive */
    {
	MPI_Recv(rbuf
		 , sz
		 , MPI_CHAR
		 , target
		 , DATA_KEY
		 , MPI_COMM_ALL
		 , MPI_STATUS_IGNORE
	    );
	MPI_Send(sbuf
		 , sz
		 , MPI_CHAR
		 , target
		 , DATA_KEY
		 , MPI_COMM_ALL
	    );
    }
}
