/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#include <mpi.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <xmmintrin.h>

#ifndef USE_REMOTE_COMM_MPI
#include <GASPI.h>
#endif

#include "SHAN_segment.h"
#include "SHAN_comm.h"
#include "SHAN_type.h"

#ifndef USE_REMOTE_COMM_MPI
#include "gaspi_util.h"
#endif

#include "shan_core.h"
#include "shan_util.h"
#include "assert.h"


void shan_comm_shmemBarrier(shan_neighborhood_t *const neighborhood_id)
{
    MPI_Barrier(neighborhood_id->MPI_COMM_SHM);
}


int shan_comm_test4Send(shan_neighborhood_t *const neighborhood_id
			, int buffer_id
			, int idx
			) 
{  

    int iProcRemote, id = -1;
    if ((iProcRemote = neighborhood_id->localRank[idx]) != -1)
    {
	int rval = -1;
	int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[idx];
	int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[idx];
	int const shm_have_read = RemoteNumNeighbors * SHAN_HAVE_READ + RemoteCommIdx;
	shan_test_shared(neighborhood_id
			 , iProcRemote
			 , RemoteNumNeighbors
			 , buffer_id
			 , shm_have_read
			 , &rval
	    );		  
	int const send_count = neighborhood_id->comm_buffer[buffer_id].local_send_count[idx];        
	if (rval >= send_count)
	{
	    __sync_synchronize();
	    ASSERT(send_count == rval);
	    id = idx;
	}
    }
    else
    {
	id = idx;
    }
    
    return (id == -1) ? -1 : SHAN_SUCCESS;
}

 

int shan_comm_wait4Send(shan_neighborhood_t *const neighborhood_id
			, int buffer_id
			, int idx
			) 
{  
    int res = -1;
    while ((res = shan_comm_test4Send(neighborhood_id
				      , buffer_id
				      , idx
		)) == -1)
    {	      
	_mm_pause();
    }	      

    return SHAN_SUCCESS;
}



int shan_comm_test4Recv(shan_neighborhood_t *const neighborhood_id
			, shan_segment_t *data_segment
			, int type_id
			, int buffer_id
			, int idx
			) 
{
    int id = -1;
    if (neighborhood_id->localRank[idx] != -1)
    {
	int res;
	if ((res = shan_comm_waitsome_local(neighborhood_id
					    , buffer_id
					    , idx
		 )) != -1)
	{
	    shan_comm_get_local(neighborhood_id
				, data_segment
				, type_id
				, idx
		); 
	    int const shm_have_read = neighborhood_id->num_neighbors * SHAN_HAVE_READ + idx;
	    shan_increment_local(neighborhood_id
				 , buffer_id
				 , shm_have_read
		);	    
	    id = idx;
	}
    }
    else
    {
	int res;
	if ((res = shan_comm_waitsome_remote(neighborhood_id
					     , type_id
					     , buffer_id
					     , idx
		 )) != -1)
	{
	    shan_comm_get_remote(neighborhood_id
				 , data_segment
				 , type_id
				 , buffer_id
				 , idx
		); 
	    id = idx;
	}
    }
  
    return (id == -1) ? -1 : SHAN_SUCCESS;
}



int shan_comm_wait4Recv(shan_neighborhood_t *const neighborhood_id
			, shan_segment_t *data_segment
			, int type_id
			, int buffer_id
			, int idx
			) 
{  
    int res = -1;
    while ((res = shan_comm_test4Recv(neighborhood_id
				      , data_segment
				      , type_id
				      , buffer_id
				      , idx
		)) == -1)
    {	      
	_mm_pause();
    }	      

    return SHAN_SUCCESS;
}


int shan_comm_wait4AllSend(shan_neighborhood_t *const neighborhood_id
			    , int buffer_id
    ) 
{
    int i, num_buff = 0;
    int const num_neighbors  = neighborhood_id->num_neighbors;

    for (i = 0; i < num_neighbors; ++i)
    {
	RESET(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
    }

    while (num_buff < num_neighbors)
    {
	for (i = 0; i < num_neighbors; ++i)
	{
	    if (!READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	    {
		int res;
		if ((res = shan_comm_test4Send(neighborhood_id
					       , buffer_id
					       , i
			 )) != -1)
		{	
		    SET_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		    num_buff++;
		}
	    }
	}
    }
  
    return SHAN_SUCCESS;
}


int shan_comm_wait4AllRecv(shan_neighborhood_t *const neighborhood_id  
			   , shan_segment_t *data_segment
			   , int type_id
			   , int buffer_id
    )
{
    int i, num_recv = 0;    
    int const num_neighbors  = neighborhood_id->num_neighbors;

    for (i = 0; i < num_neighbors; ++i)
    {
	RESET(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
    }

    while (num_recv < num_neighbors)
    {
	for (i = 0; i < num_neighbors; ++i)
	{
	    if (!READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	    {
		int res;
		if ((res = shan_comm_test4Recv(neighborhood_id
					       , data_segment
					       , type_id
					       , buffer_id
					       , i
			 )) != -1)
		{
		    SET_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		    num_recv++;
		}
	    }
	}
    }
  
    return SHAN_SUCCESS;

}


static int shan_comm_test4_read(shan_neighborhood_t *const neighborhood_id
				 , shan_segment_t *data_segment
				 , int type_id
				 , int buffer_id
				 , int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;

    if (iProcRemote != -1)
    {
	if (READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]) &&
	    !HAVE_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{
	    int res;
	    if ((res = shan_comm_waitsome_local(neighborhood_id
						, buffer_id
						, i
		     )) != -1)
	    {
		shan_comm_get_local(neighborhood_id
				    , data_segment
				    , type_id
				    , i
		    ); 
		int const shm_have_read = neighborhood_id->num_neighbors * SHAN_HAVE_READ + i;
		shan_increment_local(neighborhood_id
				     , buffer_id
				     , shm_have_read
		    );	    
		SET_HAVE_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		num++;
	    }	       
	}
    }

    return num;
}



static int shan_comm_test4_remote(shan_neighborhood_t *const neighborhood_id
				  , shan_segment_t *data_segment
				  , int type_id
				  , int buffer_id
				  , int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;

    if (iProcRemote == -1)
    {
	if (READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]) &&
	    !HAVE_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{
	    int res;
	    if ((res = shan_comm_waitsome_remote(neighborhood_id
						 , type_id
						 , buffer_id
						 , i
		     )) != -1)
	    {
		shan_comm_get_remote(neighborhood_id
				     , data_segment
				     , type_id
				     , buffer_id
				     , i
		    ); 
		SET_HAVE_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		num++;		    
	    }
	}
    }

    return num;
}


static int shan_comm_test4_write(shan_neighborhood_t *const neighborhood_id
				 , shan_segment_t *data_segment
				 , int type_id
				 , int buffer_id
				 , int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;

    if (iProcRemote != -1)
    {
	if (WRITE(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]) &&
	    !HAVE_WRITTEN(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{	    
	    int res;
	    if ((res = shan_comm_waitsome_local(neighborhood_id
						, buffer_id
						, i
		     )) != -1)
	    {
		shan_comm_write_local(neighborhood_id
				      , data_segment
				      , type_id
				      , i
		    );
		shan_segment_t *const shared_segment = &(neighborhood_id->shared_segment);
		void *const shm_ptr = shared_segment->ptr_array[iProcRemote];
		int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[i];
		int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[i];
		int const shm_have_written = RemoteNumNeighbors * SHAN_HAVE_READ + RemoteCommIdx;
		long const idxOffset = RemoteNumNeighbors * MAX_SHARED_NOTIFICATION * buffer_id + shm_have_written;
		shan_notify_increment_shared((shan_notification_t *) shm_ptr
					     , idxOffset
					     , 1
		    );        
		SET_HAVE_WRITTEN(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		num++;			
	    }		    
	}
    }	    

    return num;
}


static int shan_comm_wait4_write(shan_neighborhood_t *const neighborhood_id
				 , int buffer_id
				 , int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;
    int res;
    /*
     * wait for shm remote comm write
     */
    if (iProcRemote != -1)
    {
	if (!READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]) &&
	    !HAVE_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{
	    if ((res = shan_comm_waitsome_local(neighborhood_id
						, buffer_id
						, i
		     )) != -1)
	    {
		int const send_count = neighborhood_id->comm_buffer[buffer_id].local_send_count[i];        
		int const shm_have_written = neighborhood_id->num_neighbors * SHAN_HAVE_READ + i;
		int rval = -1;
		shan_test_shared(neighborhood_id
				 , neighborhood_id->iProcLocal
				 , neighborhood_id->num_neighbors
				 , buffer_id
				 , shm_have_written
				 , &rval
		    );		  
		if (rval >= send_count)
		{
		    __sync_synchronize();
		    ASSERT(send_count == rval);
		    SET_HAVE_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		    num++;
		}
	    }
	}
    }
    return num;
}


static int shan_comm_wait4_read(shan_neighborhood_t *const neighborhood_id
				, int buffer_id
				, int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;
    int res;

    /*
     * wait for shm remote comm read
     */
    if (iProcRemote != -1)
    {
	if (!WRITE(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]) &&
	    !HAVE_WRITTEN(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{
	    if ((res = shan_comm_waitsome_local(neighborhood_id
						, buffer_id
						, i
		     )) != -1)
	    {
		int const send_count = neighborhood_id->comm_buffer[buffer_id].local_send_count[i];        
		int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[i];
		int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[i];
		int const shm_have_written = RemoteNumNeighbors * SHAN_HAVE_READ + RemoteCommIdx;
		int rval = -1;
		shan_test_shared(neighborhood_id
				 , iProcRemote
				 , RemoteNumNeighbors
				 , buffer_id
				 , shm_have_written
				 , &rval
		    );		  
		if (rval >= send_count)
		{
		    __sync_synchronize();
		    ASSERT(send_count == rval);
		    SET_HAVE_WRITTEN(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		    num++;
		}
	    }
	}
    }

    return num;
}


static int shan_comm_flag4_read(shan_neighborhood_t *const neighborhood_id
				, int buffer_id
				, int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;
    int res;

    /*
     * try to flag shm local comm as read
     */

    if (iProcRemote != -1)
    {
	if (!READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{
	    int const send_count = neighborhood_id->comm_buffer[buffer_id].local_send_count[i];        
	    int const rval = send_count - 1;
	    int const shm_read = neighborhood_id->num_neighbors * SHAN_READ + i;
	    if ((res = shan_compare_swap(neighborhood_id
					 , neighborhood_id->iProcLocal
					 , neighborhood_id->num_neighbors
					 , buffer_id
					 , shm_read
					 , rval
		     )) == SHAN_SUCCESS)
	    {
		SET_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		num++;
	    }
	}
    }

    return num;
}


static int shan_comm_flag4_write(shan_neighborhood_t *const neighborhood_id
				 , int buffer_id
				 , int i
    )
{
    int const iProcRemote = neighborhood_id->localRank[i]; 
    int num = 0;
    int res;

    /*
     * try to flag shm remote comm as write
     */

    if (iProcRemote != -1)
    {
	if (!WRITE(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]))
	{
	    int const send_count = neighborhood_id->comm_buffer[buffer_id].local_send_count[i];        
	    int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[i];
	    int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[i];
	    int const rval = send_count - 1;
	    int const shm_write = RemoteNumNeighbors * SHAN_READ + RemoteCommIdx;
	    if ((res = shan_compare_swap(neighborhood_id
					 , iProcRemote
					 , RemoteNumNeighbors
					 , buffer_id
					 , shm_write
					 , rval
		     )) == SHAN_SUCCESS)
	    {
		SET_WRITE(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
		num++;
	    }
	}
    }

    return num;
}


int shan_comm_wait4All(shan_neighborhood_t *const neighborhood_id  
		       , shan_segment_t *const *segment_list
		       , int *type_list
		       , int num_buffer
    )
{
    int i, j;
    int res, num_have_read = 0, num_have_written = 0, num_have_remote = 0;
    int num_read = 0, num_write = 0;    
    int const num_neighbors  = neighborhood_id->num_neighbors;
    int const num_type       = neighborhood_id->num_type;
    int const num_local      = neighborhood_id->num_local;
    int const nProcLocal     = neighborhood_id->nProcLocal;
    int const iProcLocal     = neighborhood_id->iProcLocal;


    for (j = 0; j < num_buffer; ++j)
    {   
	for (i = 0; i < num_neighbors; ++i)
	{
	    RESET(neighborhood_id->comm_buffer[j].local_stage_count[i]);
	    int const iProcRemote = neighborhood_id->localRank[i]; 
	    if (iProcRemote == -1)
	    {
		SET_READ(neighborhood_id->comm_buffer[j].local_stage_count[i]);
	    }
	}
    }

    for (j = 0; j < num_buffer; ++j)
    {   
	shan_segment_t *data_segment = segment_list[j];
	int const type_id = type_list[j];
	for (i = 0; i < num_neighbors; ++i)
	{
	    num_read += shan_comm_flag4_read(neighborhood_id
					     , j
					     , i
		);
	    num_have_read += shan_comm_test4_read(neighborhood_id
						  , data_segment
						  , type_id
						  , j
						  , i
		);	    
	    num_write += shan_comm_flag4_write(neighborhood_id
					       , j
					       , i
		);
	    num_have_written += shan_comm_test4_write(neighborhood_id
						      , data_segment
						      , type_id
						      , j
						      , i
		);
	}
    }

    /*
     * try read remote node comm, shm local comm, 
     * write shm remote comm until complete
     */
    while (num_have_read < num_read || 
	   num_have_written < num_write)
    {
	for (j = 0; j < num_buffer; ++j)
	{   
            shan_segment_t *data_segment = segment_list[j];
            int const type_id = type_list[j];
	    for (i = 0; i < num_neighbors; ++i)
	    {
		num_have_read += shan_comm_test4_read(neighborhood_id
						       , data_segment
						       , type_id
						       , j
						       , i
		    );
		num_have_written += shan_comm_test4_write(neighborhood_id
							  , data_segment
							  , type_id
							  , j
							  , i
		    );
	    }
	}
    }


    while (num_have_remote < num_buffer * (num_neighbors - num_local) || 
	   num_have_read < num_buffer * num_local || 
	   num_have_written < num_buffer * num_local)
    {	
	for (j = 0; j < num_buffer; ++j)
	{   
            shan_segment_t *data_segment = segment_list[j];
            int const type_id = type_list[j];
	    for (i = 0; i < num_neighbors; ++i)
	    {
		num_have_read += shan_comm_wait4_write(neighborhood_id
						       , j
						       , i
		    );
		num_have_written += shan_comm_wait4_read(neighborhood_id
							 , j
							 , i
		    );
		num_have_remote += shan_comm_test4_remote(neighborhood_id
							  , data_segment
							  , type_id
							  , j
							  , i
		    );
	    }
	}
    }

    return SHAN_SUCCESS;


}

int shan_comm_wait4Buffer(shan_neighborhood_t *const neighborhood_id  
			  , shan_segment_t *data_segment
			  , int type_id
			  , int buffer_id
    )
{
    int const num_neighbors  = neighborhood_id->num_neighbors;
    int const num_local      = neighborhood_id->num_local;
    int i, num_have_read = 0, num_have_written = 0, num_have_remote = 0;
    int num_read = 0, num_write = 0;    

    for (i = 0; i < num_neighbors; ++i)
    {
	RESET(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
	int const iProcRemote = neighborhood_id->localRank[i]; 
	if (iProcRemote == -1)
	{
	    SET_READ(neighborhood_id->comm_buffer[buffer_id].local_stage_count[i]);
	}
    }


    for (i = 0; i < num_neighbors; ++i)
    {
	num_read += shan_comm_flag4_read(neighborhood_id
			     , buffer_id
			     , i
	    );	
	num_have_read += shan_comm_test4_read(neighborhood_id
					      , data_segment
					      , type_id
					      , buffer_id
					      , i
	    );
	num_write += shan_comm_flag4_write(neighborhood_id
					   , buffer_id
					   , i
	    );
	num_have_written += shan_comm_test4_write(neighborhood_id
						  , data_segment
						  , type_id
						  , buffer_id
						  , i
	    );
    }


    /*
     * try read remote node comm, shm local comm, 
     * write shm remote comm until complete
     */
    while (num_have_read < num_read || 
	   num_have_written < num_write)
    {
	for (i = 0; i < num_neighbors; ++i)
	{
	    num_have_read += shan_comm_test4_read(neighborhood_id
						  , data_segment
						  , type_id
						  , buffer_id
						  , i
		);
	    num_have_written += shan_comm_test4_write(neighborhood_id
						      , data_segment
						      , type_id
						      , buffer_id
						      , i
		);
	}
    }

    while (num_have_remote < (num_neighbors - num_local) ||
	   num_have_read < num_local || 
	   num_have_written < num_local)
    {	
	for (i = 0; i < num_neighbors; ++i)
	{
	    num_have_read += shan_comm_wait4_write(neighborhood_id
						   , buffer_id
						   , i
		);
	    num_have_written += shan_comm_wait4_read(neighborhood_id
						     , buffer_id
						     , i
		);
	    num_have_remote += shan_comm_test4_remote(neighborhood_id
						      , data_segment
						      , type_id
						      , buffer_id
						      , i
		);
	}
    }

    return SHAN_SUCCESS;
    
}



int shan_comm_notify_or_write(shan_neighborhood_t *const neighborhood_id
			      , shan_segment_t *const data_segment
			      , int type_id
			      , int buffer_id
			      , int idx
    )

{
    int const num_neighbors = neighborhood_id->num_neighbors;

    ASSERT(idx >= 0);
    ASSERT(idx < num_neighbors);
    
    shan_comm_notify(neighborhood_id
		     , data_segment
		     , type_id
		     , buffer_id
		     , idx
	);
    shan_comm_write(neighborhood_id
		    , data_segment
		    , type_id
		    , buffer_id
		    , idx
	);

    return SHAN_SUCCESS;
}		    


