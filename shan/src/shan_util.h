/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>

#define UP(a, b) ((((a) + (b) - 1) / (b)) * (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

#define STAGE_READ              0x01
#define STAGE_HAVE_READ         0x02
#define STAGE_WRITE             0x04
#define STAGE_HAVE_WRITTEN      0x08
#define STAGE_RESET             0x00

#define RESET(x)               (x = STAGE_RESET)

#define SET_READ(x)            (x |= STAGE_READ)
#define SET_HAVE_READ(x)       (x |= STAGE_HAVE_READ)
#define SET_WRITE(x)           (x |= STAGE_WRITE)
#define SET_HAVE_WRITTEN(x)    (x |= STAGE_HAVE_WRITTEN)

#define UNSET_READ(x)          (x &= (~STAGE_READ))
#define UNSET_HAVE_READ(x)     (x &= (~STAGE_HAVE_READ))
#define UNSET_WRITE(x)         (x &= (~STAGE_WRITE))
#define UNSET_HAVE_WRITTEN(x)  (x &= (~STAGE_HAVE_WRITTEN))

#define TOGGLE_READ(x)         (x ^= STAGE_READ)
#define TOGGLE_HAVE_READ(x)    (x ^= STAGE_HAVE_READ)
#define TOGGLE_WRITE(x)        (x ^= STAGE_WRITE)
#define TOGGLE_HAVE_WRITTEN(x) (x ^= STAGE_HAVE_WRITTEN)

#define READ(x)                (x & STAGE_READ)
#define HAVE_READ(x)           (x & STAGE_HAVE_READ)
#define WRITE(x)               (x & STAGE_WRITE)
#define HAVE_WRITTEN(x)        (x & STAGE_HAVE_WRITTEN)


void check_free(void *ptr);

void *check_malloc(long bytes);

#endif
