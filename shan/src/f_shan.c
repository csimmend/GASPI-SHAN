/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#include <mpi.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

#ifndef USE_REMOTE_COMM_MPI
#include <GASPI.h>
#endif

#include "SHAN_segment.h"
#include "SHAN_comm.h"
#include "SHAN_type.h"
#include "F_SHAN.h"

#include "assert.h"
#include "shan_util.h"
 
#define NUM_SOCKETS 1
 
static shan_neighborhood_t neighbor_hood[MAX_NEIGHBOR_HOOD];
static shan_segment_t data_segment[MAX_SEGMENT];
static shan_segment_t *segment_ptr[MAX_SEGMENT];

static int gaspi_initialized = 0;
static int shmem_initialized = 0;
static MPI_Comm MPI_COMM_SHM;

void f_shan_init_gaspi(void)
{

#ifndef USE_REMOTE_COMM_MPI
    ASSERT(gaspi_initialized == 0);

    int ierr, mype = -1, numpes = 0;
    ierr = MPI_Comm_rank ( MPI_COMM_WORLD, &mype );
    ierr = MPI_Comm_size ( MPI_COMM_WORLD, &numpes );
    
    gaspi_config_t config;
    SUCCESS_OR_DIE (gaspi_config_get(&config));
    config.build_infrastructure = GASPI_TOPOLOGY_NONE;
    SUCCESS_OR_DIE (gaspi_config_set(config));

    gaspi_rank_t iProc, nProc;
    SUCCESS_OR_DIE (gaspi_proc_init (GASPI_BLOCK));
    SUCCESS_OR_DIE (gaspi_proc_rank (&iProc));
    SUCCESS_OR_DIE (gaspi_proc_num (&nProc));

    ASSERT(iProc == mype);
    ASSERT(nProc == numpes); 

    gaspi_initialized = 1;
#endif

}

void f_shan_alloc_shared( const int segment_id
			  , const long dataSz
			  , void **restrict shm_ptr
    )
{
  ASSERT(segment_id < MAX_SEGMENT);
  int res, iProcLocal;

  if (!shmem_initialized)
  {
#ifndef NUM_SOCKETS
      MPI_Comm_split_type (MPI_COMM_WORLD
			   , MPI_COMM_TYPE_SHARED
			   , 0
			   , MPI_INFO_NULL
			   , &MPI_COMM_SHM
	  );
#else
      MPI_Comm COMM_SHM;
      MPI_Comm_split_type (MPI_COMM_WORLD
			   , MPI_COMM_TYPE_SHARED
			   , 0
			   , MPI_INFO_NULL
			   , &COMM_SHM
	  );
      int iProc, nProc;
      MPI_Comm_rank(COMM_SHM, &iProc);
      MPI_Comm_size(COMM_SHM, &nProc);

      MPI_Comm_split (COMM_SHM
		      , NUM_SOCKETS * iProc / nProc
		      , iProc
		      , &MPI_COMM_SHM
	  );
#endif

      shmem_initialized = 1;
  }

  MPI_Comm_rank(MPI_COMM_SHM, &iProcLocal);
  shan_segment_t *dataSegment  = &data_segment[segment_id];
  res = shan_alloc_shared(dataSegment
			  , segment_id
			  , SHAN_DATA
			  , dataSz			  
			  , MPI_COMM_SHM 
			  );
  ASSERT(res == SHAN_SUCCESS);
  res = shan_get_shared_ptr(dataSegment
			    , iProcLocal
			    , shm_ptr
			    );
  ASSERT(res == SHAN_SUCCESS);


}


void f_shan_free_shared(const int segment_id)
{
  ASSERT(segment_id < MAX_SEGMENT);

  int res;
  shan_segment_t *dataSegment  = &data_segment[segment_id];
  res = shan_free_shared(dataSegment);
  ASSERT(res == SHAN_SUCCESS);

}


void f_shan_free_comm(const int neighbor_hood_id)
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);

  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  int res = shan_comm_free_comm(ngbSegment);
  ASSERT(res == SHAN_SUCCESS);

  MPI_Comm_free(&(ngbSegment->MPI_COMM_SHM));
    
}

void f_shan_init_comm(const int neighbor_hood_id
		      , void *neighbors
		      , int num_neighbors
		      , void *max_nelem_send
		      , void *max_nelem_recv
		      , int num_type
		      , void *maxSendSz
		      , void *maxRecvSz
		      , int num_buffer
		      )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);

  int res, iProcLocal;
  if (!shmem_initialized)
  {
#ifndef NUM_SOCKETS
      MPI_Comm_split_type (MPI_COMM_WORLD
			   , MPI_COMM_TYPE_SHARED
			   , 0
			   , MPI_INFO_NULL
			   , &MPI_COMM_SHM
	  );

#else
      MPI_Comm COMM_SHM;
      MPI_Comm_split_type (MPI_COMM_WORLD
			   , MPI_COMM_TYPE_SHARED
			   , 0
			   , MPI_INFO_NULL
			   , &COMM_SHM
	  );
      int iProc, nProc;
      MPI_Comm_rank(COMM_SHM, &iProc);
      MPI_Comm_size(COMM_SHM, &nProc);

      MPI_Comm_split (COMM_SHM
		      , NUM_SOCKETS * iProc / nProc
		      , iProc
		      , &MPI_COMM_SHM
	  );
#endif

      shmem_initialized = 1;
  }

  MPI_Comm_rank(MPI_COMM_SHM, &iProcLocal);
  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];

  res = shan_comm_init_comm(ngbSegment
			    , neighbor_hood_id
			    , (int*) neighbors
			    , num_neighbors
			    , (int*) max_nelem_send
			    , (int*) max_nelem_recv
			    , num_type
			    , (long*) maxSendSz
			    , (long*) maxRecvSz			    
			    , num_buffer
			    , MPI_COMM_SHM
			    , MPI_COMM_WORLD
			    );  
  ASSERT(res == SHAN_SUCCESS);  
}


void f_shan_set_data_layout(int const segment_id
			    , long const d1
			    , long const d2
			    , int type_layout
    )
{
    ASSERT(segment_id < MAX_SEGMENT);
    shan_segment_t *dataSegment  = &data_segment[segment_id];
    shan_set_data_layout(dataSegment
			 , d1
			 , d2
			 , type_layout
	);    
}

void f_shan_type_layout(const int neighbor_hood_id
			, const int type_id
			, void **nelem_send
			, void **nelem_recv
			, void **nelem_per_send
			, void **nelem_per_recv
			, void **send_idx
			, void **recv_idx
    )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
  
  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  int res = shan_type_layout(ngbSegment
			     , type_id
			     , (int**) nelem_send
			     , (int**) nelem_recv
			     , (int**) nelem_per_send
			     , (int**) nelem_per_recv
			     , (long**) send_idx
			     , (long**) recv_idx
      );
  ASSERT(res == SHAN_SUCCESS);  
}


void f_shan_comm_wait4All(const int neighbor_hood_id  
			  , void *f_segment_list
			  , void *f_type_list
			  , int num_comm
    )
{
    int i;
    ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
    shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
    int *seg_id = (int*) f_segment_list;

    for (i = 0; i < num_comm; ++i)
    {
	ASSERT(seg_id[i] < MAX_SEGMENT);
	segment_ptr[i] = &data_segment[seg_id[i]];
    }
	
    int res = shan_comm_wait4All(ngbSegment
				 , segment_ptr
				 , (int*) f_type_list
				 , num_comm
	);
    ASSERT(res == SHAN_SUCCESS);
}


 
void f_shan_comm_wait4Buffer(const int neighbor_hood_id  
			     , const int segment_id
			     , const int type_id
			     , const int buffer_id
    )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
  ASSERT(segment_id < MAX_SEGMENT);

  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  shan_segment_t *dataSegment  = &data_segment[segment_id];
  
  int res = shan_comm_wait4Buffer(ngbSegment
				  , dataSegment
				  , type_id
				  , buffer_id
      );
  ASSERT(res == SHAN_SUCCESS);
}


int f_shan_comm_local_rank(const int neighbor_hood_id
			    , int idx
    )
{
    ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
    shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
    return shan_comm_local_rank(ngbSegment, idx);
}


void f_shan_comm_notify_or_write(const int neighbor_hood_id
				 , const int segment_id
				 , const int type_id
				 , const int buffer_id
				 , int idx
    )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
  ASSERT(segment_id < MAX_SEGMENT);

  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  shan_segment_t *dataSegment  = &data_segment[segment_id];

  int res = shan_comm_notify_or_write(ngbSegment
				      , dataSegment
				      , type_id				      
				      , buffer_id
				      , idx
      );
  ASSERT(res == SHAN_SUCCESS);  
  
}

void f_shan_comm_write(const int neighbor_hood_id
		       , const int segment_id
		       , const int type_id
		       , const int buffer_id
		       , int idx
    )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
  ASSERT(segment_id < MAX_SEGMENT);

  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  shan_segment_t *dataSegment  = &data_segment[segment_id];

  int res = shan_comm_write(ngbSegment
			    , dataSegment
			    , type_id
			    , buffer_id
			    , idx
      );
  ASSERT(res == SHAN_SUCCESS);  
  
}

void f_shan_comm_notify(const int neighbor_hood_id
			, const int segment_id
			, const int type_id
			, const int buffer_id
			, int idx
    )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
  ASSERT(segment_id < MAX_SEGMENT);

  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  shan_segment_t *dataSegment  = &data_segment[segment_id];

  int res = shan_comm_notify(ngbSegment
			     , dataSegment
			     , type_id
			     , buffer_id
			     , idx
      );
  ASSERT(res == SHAN_SUCCESS);  
  
}

void f_shan_comm_post_recv(const int neighbor_hood_id
			   , const int segment_id
			   , const int type_id
			   , const int buffer_id
			   , int idx
    )
{
  ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
  ASSERT(segment_id < MAX_SEGMENT);

  shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
  shan_segment_t *dataSegment  = &data_segment[segment_id];

  int res = shan_comm_post_recv(ngbSegment
				, dataSegment
				, type_id
				, buffer_id
				, idx
      );
  ASSERT(res == SHAN_SUCCESS);  
  
}


int f_shan_get_segment_id(void *shm_ptr)
{
    int segment_id = shan_get_segment_id(shm_ptr);
    ASSERT(segment_id < MAX_SEGMENT);

    shan_segment_t *dataSegment  = &data_segment[segment_id];
    ASSERT(dataSegment->shan_id == segment_id);

    return segment_id;
}


long f_shan_alloc_get_default_mem_sz(void)
{
  if (!shmem_initialized)
  {
#ifndef NUM_SOCKETS
      MPI_Comm_split_type (MPI_COMM_WORLD
			   , MPI_COMM_TYPE_SHARED
			   , 0
			   , MPI_INFO_NULL
			   , &MPI_COMM_SHM
	  );
#else
      MPI_Comm COMM_SHM;
      MPI_Comm_split_type (MPI_COMM_WORLD
			   , MPI_COMM_TYPE_SHARED
			   , 0
			   , MPI_INFO_NULL
			   , &COMM_SHM
	  );
      int iProc, nProc;
      MPI_Comm_rank(COMM_SHM, &iProc);
      MPI_Comm_size(COMM_SHM, &nProc);

      MPI_Comm_split (COMM_SHM
		      , NUM_SOCKETS * iProc / nProc
		      , iProc
		      , &MPI_COMM_SHM
	  );
#endif

      shmem_initialized = 1;
  }
  return shan_alloc_get_default_mem_sz(MPI_COMM_SHM);
}


void f_shan_type_validate(const int neighbor_hood_id
			, int type_id
    )
{
    ASSERT(neighbor_hood_id < MAX_NEIGHBOR_HOOD);
    shan_neighborhood_t *ngbSegment = &neighbor_hood[neighbor_hood_id];
    shan_type_validate(ngbSegment, type_id);
}


void f_shan_track_ptr_name(void **ptr)
{
    shan_track_ptr_name(*ptr);
}
