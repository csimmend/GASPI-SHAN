/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#ifndef SHAN_COPY_H
#define SHAN_COPY_H

#include <stdio.h>
#include <stdlib.h>

int shan_copy_get_type_sz(int layout);

void shan_copy_src_dest_AoS( int  nelem_send
			     , int  nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , long *recv_index 
			     , void *recv_ptr
			     , long d1
			     , int layout
    );

void shan_copy_src_comm_AoS( int  nelem_send
			     , int  nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , void *comm_ptr
			     , long d1
			     , int layout
    );

void shan_copy_comm_dest_AoS( void *comm_ptr
			      , int nelem_recv
			      , int  nelem_per_recv
			      , long *recv_index 
			      , void *recv_ptr
			      , long d1
			      , int layout
    );

void shan_copy_src_dest_SoA( int  nelem_send
			     , int  nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , long *recv_index 
			     , void *recv_ptr
			     , long d1_send
			     , long d2_send
			     , long d2_recv
			     , int layout
    );

void shan_copy_src_comm_SoA( int  nelem_send
			     , int  nelem_per_send
			     , long *send_index 
			     , void *send_ptr
			     , void *comm_ptr
			     , long d1
			     , long d2
			     , int layout
    );

void shan_copy_comm_dest_SoA( void *comm_ptr
			      , int nelem_recv
			      , int  nelem_per_recv
			      , long *recv_index 
			      , void *recv_ptr
			      , long d1
			      , long d2
			      , int layout
    );

#endif
