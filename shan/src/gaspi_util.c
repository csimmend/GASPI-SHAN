/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#include <stdio.h>
#ifndef USE_REMOTE_COMM_MPI
#include "GASPI.h"
#include "GASPI_Ext.h"
#endif
#include "assert.h"

#ifndef USE_REMOTE_COMM_MPI
void
write_notify_and_wait ( gaspi_segment_id_t segment_id
			, gaspi_offset_t const offset_local
			, gaspi_rank_t const rank
			, gaspi_offset_t const offset_remote
			, gaspi_size_t const size
			, gaspi_notification_id_t const notification_id
			, gaspi_notification_t const notification_value
			, gaspi_queue_id_t const queue
			)
{
  gaspi_timeout_t const timeout = GASPI_BLOCK;
  gaspi_return_t ret;
  
  /* write, wait if required and re-submit */
  while ((ret = ( gaspi_write_notify( segment_id
				      , offset_local
				      , rank
				      , segment_id
				      , offset_remote
				      , size
				      , notification_id
				      , notification_value
				      , queue
				      , timeout
				      )
		  )) == GASPI_QUEUE_FULL)
    {
      SUCCESS_OR_DIE (gaspi_wait (queue,
				  GASPI_BLOCK));
    }

  ASSERT (ret == GASPI_SUCCESS);
}

#endif
