/*
 * Copyright [2018] [Christian Simmendinger, T-Systems SfR]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



#include <mpi.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <xmmintrin.h>

#ifndef USE_REMOTE_COMM_MPI
#include <GASPI.h>
#endif

#include "SHAN_segment.h"
#include "SHAN_comm.h"
#include "SHAN_type.h"

#include "shan_core.h"
#ifndef USE_REMOTE_COMM_MPI
#include "gaspi_util.h"
#endif
#include "shan_util.h"
#include "shan_copy.h"
#include "assert.h"
#include <sys/time.h>


#ifndef USE_REMOTE_COMM_MPI
#ifndef USE_NOCOS
#define GASPI_PROC_LOCAL 0
#endif
#endif

#define GET_NOTIFICATION_ID(sid, num_buffer, buffer_id, num_neighbors, idx) \
  ((sid) * ((num_buffer) * (num_neighbors)) + (buffer_id) * (num_neighbors) + (idx))  



static int shan_alloc_remote(shan_remote_t * const segment   
			     , int const shan_id
			     , const long dataSz
			     , MPI_Comm MPI_COMM_SHM
    )
{
    ASSERT(segment != NULL);
    segment->shan_id = shan_id;
    segment->dataSz = dataSz;

    if (dataSz >0)
      {
	  long const page_size = sysconf (_SC_PAGESIZE);
	  int res = posix_memalign(&(segment->shan_ptr), page_size, dataSz);
	  ASSERT(res == 0);
	  ASSERT(segment->shan_ptr != NULL);
      }
    else
      {       
	segment->shan_ptr = NULL;
      }
    return SHAN_SUCCESS; 
}


static int shan_free_remote(shan_remote_t * const segment)
{
    ASSERT(segment != NULL);
    check_free(segment->shan_ptr);
  
    return SHAN_SUCCESS; 
}


#ifndef USE_REMOTE_COMM_MPI
static void bind_to_segment(shan_neighborhood_t *const neighborhood_id
			    , const gaspi_segment_id_t segment_id
			    )
{
    ASSERT(neighborhood_id != NULL);
    /* 
     * we bind to the entire comm segment at rank 0 
     * includes all (page aligned) send / recv buffers 
     * from all local ranks
     */
  

    shan_remote_t *remote_segment = &(neighborhood_id->remote_segment);
    gaspi_pointer_t seg_ptr = (gaspi_pointer_t *) remote_segment->shan_ptr;
    gaspi_size_t CommSz = (gaspi_size_t) remote_segment->dataSz;
	 
    /* 
     * bind to shared GASPI segment 
     */
    if (CommSz > 0)
    {
	SUCCESS_OR_DIE (gaspi_segment_bind( (gaspi_segment_id_t) segment_id 
					    , seg_ptr
					    , CommSz
					    , GASPI_PROC_LOCAL
			    ));
    }
    MPI_Barrier(neighborhood_id->MPI_COMM_ALL);

    int i, j;
    for (i = 0; i < neighborhood_id->max_num_neighbors; ++i)
    {
	for (j = 0; j < neighborhood_id->nProcLocal; ++j)
	{
	    if (neighborhood_id->iProcLocal == j)
	    {
		if(neighborhood_id->localRank[i] == -1 
		   && i < neighborhood_id->num_neighbors && CommSz > 0)
		{
		    int const rank = neighborhood_id->neighbors[i];
		    SUCCESS_OR_DIE( gaspi_connect (rank, GASPI_BLOCK));
		    SUCCESS_OR_DIE( gaspi_segment_register((gaspi_segment_id_t) segment_id
							   , rank
							   , GASPI_BLOCK
					));
		}
	    }
	    MPI_Barrier(neighborhood_id->MPI_COMM_SHM);
	}
    }

}
#endif

static void shan_comm_alloc_comm(shan_neighborhood_t *const neighborhood_id)
{
    int i;
    ASSERT(neighborhood_id != NULL);

    int const segment_id = neighborhood_id->neighbor_hood_id;

#ifndef USE_REMOTE_COMM_MPI
    gaspi_number_t  segment_max;
    SUCCESS_OR_DIE (gaspi_segment_max (&segment_max));

    gaspi_number_t segment_num;
    SUCCESS_OR_DIE(gaspi_segment_num(&segment_num));  

    ASSERT(segment_id < segment_max);

    if (segment_num > 0)
    {
	gaspi_segment_id_t *segment_list =
	    check_malloc(segment_num * sizeof(gaspi_segment_id_t));
	SUCCESS_OR_DIE(gaspi_segment_list (segment_num, segment_list));
	for (i = 0; i < (int) segment_num; ++i)
	{
	    ASSERT(segment_list[i] != segment_id);
	}
	check_free(segment_list);
    }
#endif

    int res;
    /*
     * alloc comm space remote
     */
    long sz_remote = neighborhood_id->remoteSz;
    shan_remote_t *remote_segment  = &(neighborhood_id->remote_segment);
    res = shan_alloc_remote(remote_segment
			    , segment_id
			    , sz_remote
			    , neighborhood_id->MPI_COMM_SHM
	);      
    ASSERT(res == SHAN_SUCCESS);

    if (sz_remote > 0)
    {
	memset((char*) remote_segment->shan_ptr
	       , 0
	       , sz_remote
	    );
    }

#ifndef USE_REMOTE_COMM_MPI
    /*
     * bind remote comm as GASPI segment
     */
    bind_to_segment(neighborhood_id, segment_id);

    gaspi_number_t notification_num;
    SUCCESS_OR_DIE(gaspi_notification_num (&notification_num));
    int max_notifications = 2 * neighborhood_id->num_neighbors
	* neighborhood_id->num_buffer;

    ASSERT(max_notifications < (int) notification_num);
    for (i = 0; i < max_notifications ; ++i)
    {
	gaspi_notification_t nval;
	SUCCESS_OR_DIE(gaspi_notify_reset (remote_segment->shan_id
					   , (gaspi_notification_id_t) i
					   , &nval
			   ));
    }
#endif

    MPI_Barrier(neighborhood_id->MPI_COMM_ALL);

}

int shan_comm_local_rank(shan_neighborhood_t * const neighborhood_id
		    , int const idx
		    )
{
    return neighborhood_id->localRank[idx];
}


static void shan_negotiate_meta_data(shan_neighborhood_t * const neighborhood_id)
{
    int i;
    int num_neighbors = neighborhood_id->num_neighbors;
    /* 
     * exchange remote num_neighbors
     */	  
    int *nngb = check_malloc (neighborhood_id->nProcGlobal * sizeof(int));  
    MPI_Allgather(&(neighborhood_id->num_neighbors)
		  , 1
		  , MPI_INT
		  , nngb
		  , 1
		  , MPI_INT
		  , neighborhood_id->MPI_COMM_ALL
	);  

    neighborhood_id->RemoteNumNeighbors = check_malloc(num_neighbors * sizeof(int));
    for (i = 0; i < num_neighbors; ++i)
    {
	int const rank = neighborhood_id->neighbors[i];
	neighborhood_id->RemoteNumNeighbors[i] = nngb[rank];
    }	  

    /* 
     * exchange remote neighbors
     */	  
    int *disp = check_malloc (neighborhood_id->nProcGlobal * sizeof(int));  
    int ncomm = 0;
    for (i = 0; i < neighborhood_id->nProcGlobal; ++i)
    {
	disp[i] = ncomm;
	ncomm += nngb[i];
    }	  

    int *buff = check_malloc (ncomm * sizeof(int));  
    for (i = 0; i < ncomm; ++i)
    {
	buff[i] = -1;
    }
    MPI_Allgatherv(neighborhood_id->neighbors
		   , neighborhood_id->num_neighbors
		   , MPI_INT
		   , buff
		   , nngb
		   , disp
		   , MPI_INT
		   , neighborhood_id->MPI_COMM_ALL
	);  
    for (i = 0; i < ncomm; ++i)
    {
	ASSERT(buff[i] != -1);
    }

    /* 
     * exchange remote comm idx
     */	  
    neighborhood_id->RemoteCommIndex = check_malloc(num_neighbors * sizeof(int));
    for (i = 0; i < num_neighbors; ++i)
    {
	neighborhood_id->RemoteCommIndex[i] = -1;	  
    }
  
    for (i = 0; i < num_neighbors; ++i)
    {
	int const rank = neighborhood_id->neighbors[i];
	int j, bdir = 0;
	for (j = 0; j < nngb[rank]; ++j)
	{
	    if (buff[disp[rank]+j] == neighborhood_id->iProcGlobal)
	    {
		neighborhood_id->RemoteCommIndex[i] = j;  
		buff[disp[rank]+j] = -1;
		bdir = 1;
		break;
	    }
	}
	ASSERT(bdir == 1);
    }
    for (i = 0; i < ncomm; ++i)
    {
	ASSERT(buff[i] != neighborhood_id->iProcGlobal);
    }

    check_free(buff);
    check_free(disp);
    check_free(nngb);

}


int shan_comm_free_comm(shan_neighborhood_t *const neighborhood_id)
{
    int i;
    for (i = 0; i < neighborhood_id->num_buffer; ++i)
    {
	check_free(neighborhood_id->comm_buffer[i].local_send_count);
	check_free(neighborhood_id->comm_buffer[i].local_stage_count);
#ifdef USE_REMOTE_COMM_MPI
	check_free(neighborhood_id->comm_buffer[i].recv_req);
	check_free(neighborhood_id->comm_buffer[i].send_req);
#endif
    }
    check_free(neighborhood_id->comm_buffer);    
    check_free(neighborhood_id->type_element);

    check_free(neighborhood_id->neighbors);
    check_free(neighborhood_id->remote_master);
    check_free(neighborhood_id->localRank);
    check_free(neighborhood_id->RemoteNumNeighbors);
    check_free(neighborhood_id->RemoteCommIndex );

    shan_segment_t *shared_segment = &(neighborhood_id->shared_segment);
    shan_free_shared(shared_segment);

#ifndef USE_REMOTE_COMM_MPI
    SUCCESS_OR_DIE (gaspi_wait (0, GASPI_BLOCK));
    MPI_Barrier(neighborhood_id->MPI_COMM_ALL);
    SUCCESS_OR_DIE(gaspi_segment_delete(neighborhood_id->neighbor_hood_id));
#endif

    shan_remote_t *remote_segment  = &(neighborhood_id->remote_segment);
    shan_free_remote(remote_segment);

    return SHAN_SUCCESS;

}

static void shan_comm_wait_for_debugger(int iProcGlobal)
{
    volatile int i = 0;
    if(getenv("SHAN_PARALLEL_DEBUG") != NULL && iProcGlobal == 0) 
    {
        fprintf(stderr , "pid %ld - waiting for debugger\n"
                , (long) getpid ()
            );
        while(i == 0) { /*  change  ’i’ in the  debugger  */ }
    }
    MPI_Barrier(MPI_COMM_WORLD);
}

int shan_comm_init_comm(shan_neighborhood_t *const neighborhood_id
			, int neighbor_hood_id
			, int *neighbors
			, int num_neighbors
			, int *max_nelem_send
			, int *max_nelem_recv
			, int num_type 
			, long *maxSendSz
			, long *maxRecvSz
			, int num_buffer
			, MPI_Comm MPI_COMM_SHM
			, MPI_Comm MPI_COMM_ALL
			)
{
    int i, j, k;
    ASSERT(neighborhood_id != NULL);
    ASSERT(neighbors != NULL);
    ASSERT(num_neighbors > 0);

    ASSERT(maxSendSz != NULL);
    ASSERT(maxRecvSz != NULL);
    ASSERT(max_nelem_send != NULL);
    ASSERT(max_nelem_recv != NULL);
    ASSERT(num_type > 0);
    ASSERT(num_buffer> 0);

    int initialized = 0;
    MPI_Initialized(&initialized);
    ASSERT(initialized != 0);

    ASSERT(MPI_COMM_SHM != MPI_COMM_NULL);
    ASSERT(MPI_COMM_ALL != MPI_COMM_NULL);

    neighborhood_id->neighbor_hood_id = neighbor_hood_id;
    neighborhood_id->num_type         = num_type;
    neighborhood_id->num_buffer       = num_buffer;
    neighborhood_id->num_neighbors    = num_neighbors;

    neighborhood_id->MPI_COMM_SHM = MPI_COMM_SHM;
    neighborhood_id->MPI_COMM_ALL = MPI_COMM_ALL;
  
    MPI_Comm_rank(MPI_COMM_SHM, &(neighborhood_id->iProcLocal));
    MPI_Comm_size(MPI_COMM_SHM, &(neighborhood_id->nProcLocal));

    MPI_Comm_rank(MPI_COMM_ALL, &(neighborhood_id->iProcGlobal));
    MPI_Comm_size(MPI_COMM_ALL, &(neighborhood_id->nProcGlobal));
    
    /*
     * optional debug start
     */
    shan_comm_wait_for_debugger(neighborhood_id->iProcGlobal);

    /*
     * neighbors
     */
    neighborhood_id->neighbors = check_malloc(num_neighbors * sizeof(int));  
    for (i = 0; i < num_neighbors; ++i)
    {
	ASSERT(neighbors[i] >= 0);
	neighborhood_id->neighbors[i] = neighbors[i];
    }
    neighborhood_id->max_num_neighbors = num_neighbors;
    MPI_Allreduce( MPI_IN_PLACE
		   , &neighborhood_id->max_num_neighbors
		   , 1
		   , MPI_INT
		   , MPI_MAX
		   , neighborhood_id->MPI_COMM_ALL
	);

    /*
     * local master
     */
    neighborhood_id->master = neighborhood_id->iProcGlobal;
    MPI_Bcast(&(neighborhood_id->master)
	      , 1
	      , MPI_INT
	      , 0
	      , neighborhood_id->MPI_COMM_SHM
	);
  

    /*
     * validate homogeneous rank distribution
     */
    int minProcLocal = neighborhood_id->nProcLocal;
    MPI_Allreduce( MPI_IN_PLACE
		   , &minProcLocal
		   , 1
		   , MPI_INT
		   , MPI_MAX
		   , neighborhood_id->MPI_COMM_ALL
	);
    ASSERT(minProcLocal == neighborhood_id->nProcLocal);

    int maxProcLocal = neighborhood_id->nProcLocal;
    MPI_Allreduce( MPI_IN_PLACE
		   , &maxProcLocal
		   , 1
		   , MPI_INT
		   , MPI_MIN
		   , neighborhood_id->MPI_COMM_ALL
	);
    ASSERT(maxProcLocal == neighborhood_id->nProcLocal);
  
    /*
     * remote masters
     */
    neighborhood_id->remote_master
	= check_malloc (neighborhood_id->nProcGlobal * sizeof(int));  
    MPI_Allgather(&(neighborhood_id->master)
		  , 1
		  , MPI_INT
		  , neighborhood_id->remote_master
		  , 1
		  , MPI_INT
		  , neighborhood_id->MPI_COMM_ALL
	);  
    ASSERT(neighborhood_id->remote_master[neighborhood_id->iProcGlobal]
	   == neighborhood_id->master);
  
    /*
     * local ranks
     */
    neighborhood_id->num_local = 0;
    neighborhood_id->localRank
	= check_malloc (neighborhood_id->num_neighbors * sizeof(int));
    for (i = 0; i < neighborhood_id->num_neighbors; ++i)
    {
	int val = -1;	
	int const rank = neighborhood_id->neighbors[i];
	if (neighborhood_id->master == neighborhood_id->remote_master[rank])
	{
	    neighborhood_id->num_local++;
	    val = rank % neighborhood_id->nProcLocal;
	}    
	neighborhood_id->localRank[i] = val;
    }  

    /*
     * negotiate remote comm index
     */
    shan_negotiate_meta_data(neighborhood_id);

  
    /* 
     * global max comm sizes 
     */
    MPI_Allreduce( MPI_IN_PLACE
		   , maxSendSz
		   , num_buffer
		   , MPI_LONG
		   , MPI_MAX
		   , neighborhood_id->MPI_COMM_ALL
	);

    MPI_Allreduce( MPI_IN_PLACE
		   , maxRecvSz
		   , num_buffer
		   , MPI_LONG
		   , MPI_MAX
		   , neighborhood_id->MPI_COMM_ALL
	);
  
    /* 
     * global max elements
     */
    MPI_Allreduce( MPI_IN_PLACE
		   , max_nelem_send
		   , num_type
		   , MPI_INT
		   , MPI_MAX
		   , neighborhood_id->MPI_COMM_ALL
	);

    MPI_Allreduce( MPI_IN_PLACE
		   , max_nelem_recv
		   , num_type
		   , MPI_INT
		   , MPI_MAX
		   , neighborhood_id->MPI_COMM_ALL
	);

    /*
     * local comm 
     */
    int const page_size = sysconf (_SC_PAGESIZE);
    neighborhood_id->type_element
	= check_malloc(num_type * sizeof(shan_element_t));

    for (i = 0; i < num_type; ++i)
    {
	neighborhood_id->type_element[i].max_nelem_send = max_nelem_send[i];
	neighborhood_id->type_element[i].max_nelem_recv = max_nelem_recv[i];
    }
      
    long elemOffset = num_buffer 
	* MAX_SHARED_NOTIFICATION * sizeof(shan_notification_t);

    for (i = 0; i < num_type; ++i)
    {
	neighborhood_id->type_element[i].elemOffset = elemOffset;
	elemOffset += 4 * sizeof(int) + (max_nelem_send[i] + max_nelem_recv[i]) * sizeof(long);
    }
    
    long const typeOffset = UP(num_neighbors * elemOffset, page_size);
    shan_segment_t *shared_segment = &(neighborhood_id->shared_segment);
    int res = shan_alloc_shared(shared_segment
				, neighborhood_id->neighbor_hood_id
				, SHAN_TYPE
				, typeOffset
				, neighborhood_id->MPI_COMM_SHM
	);
    ASSERT(res == SHAN_SUCCESS);


    /*
    /* 
     * shared mem notifcations
     */
    for (i = 0; i < num_buffer; ++i)
    {
	for (k = 0; k < num_neighbors * MAX_SHARED_NOTIFICATION; ++k)
	{
	    shan_init_local(neighborhood_id
			    , i
			    , k
			    , 0 
		);		
	}
    }


    /*
     * types
     */
    for (i = 0; i < num_type; ++i)
    {
	type_local_t type_info;
	shan_get_shared_type(&type_info
			     , neighborhood_id
			     , neighborhood_id->iProcLocal
			     , neighborhood_id->num_neighbors
			     , i
	    );

	for (k = 0; k < num_neighbors; ++k)
	{
	    type_info.nelem_send[k]        = 0;
	    type_info.nelem_recv[k]        = 0;
	    type_info.nelem_per_send[k]    = 0;
	    type_info.nelem_per_recv[k]    = 0;
	}
      
	for (k = 0; k < num_neighbors * max_nelem_send[i]; ++k)
	{
	    type_info.send_offset_index[k] = 0;
	}
      
	for (k = 0; k < num_neighbors * max_nelem_recv[i]; ++k)
	{
	    type_info.recv_offset_index[k] = 0;
	}

    }

    
    /*
     * remote comm
     */
    neighborhood_id->comm_buffer
	= check_malloc(num_buffer * sizeof(shan_buffer_t));    
  
    for (i = 0; i < num_buffer; ++i)
    {
	neighborhood_id->comm_buffer[i].local_send_count
	    = check_malloc(num_neighbors *sizeof(int));
	neighborhood_id->comm_buffer[i].local_stage_count 
	    = check_malloc(num_neighbors * sizeof(unsigned char));
	  
	for (j = 0; j < num_neighbors; ++j)
	{
	    neighborhood_id->comm_buffer[i].local_send_count[j]  = 0;
	    neighborhood_id->comm_buffer[i].local_stage_count[j] = 0;
	}

#ifdef USE_REMOTE_COMM_MPI
        neighborhood_id->comm_buffer[i].recv_req 
            = check_malloc(2 * num_neighbors * sizeof(MPI_Request));
        neighborhood_id->comm_buffer[i].send_req 
            = check_malloc(2 * num_neighbors * sizeof(MPI_Request));
#endif

    }

    long remoteSz = 0;
    for (i = 0; i < num_buffer; ++i)
    {
	long sz = maxSendSz[i] + NELEM_COMM_HEADER * sizeof(int);
	sz = UP(sz, ALIGNMENT);
	neighborhood_id->comm_buffer[i].maxSendSz    = sz;
	neighborhood_id->comm_buffer[i].SendOffset[0] = remoteSz;
	neighborhood_id->comm_buffer[i].SendOffset[1] = remoteSz + sz;
	remoteSz += 2 * sz;
    }
    for (i = 0; i < num_buffer; ++i)
    {
	long sz = maxRecvSz[i] + NELEM_COMM_HEADER * sizeof(int);
	sz = UP(sz, ALIGNMENT);
	neighborhood_id->comm_buffer[i].maxRecvSz    = sz;
	neighborhood_id->comm_buffer[i].RecvOffset[0] = remoteSz;
	neighborhood_id->comm_buffer[i].RecvOffset[1] = remoteSz + sz;
	remoteSz += 2 * sz;
    }	  
    neighborhood_id->remoteSz = UP(num_neighbors * remoteSz, page_size);

  
    /*
     * allocate and bind remote comm segment 
     */
    shan_comm_alloc_comm(neighborhood_id);

    return SHAN_SUCCESS;
}


int shan_comm_waitsome_local(shan_neighborhood_t *const neighborhood_id
			     , int const buffer_id
			     , int const idx
    )
{
    int id = -1;  
    int const num_neighbors = neighborhood_id->num_neighbors;    
    int const iProcRemote = neighborhood_id->localRank[idx]; 
    int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[idx];
    int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[idx];
    int const shm_notify = RemoteNumNeighbors * SHAN_NOTIFY + RemoteCommIdx;
    int rval = -1;
    shan_test_shared(neighborhood_id
		     , iProcRemote
		     , RemoteNumNeighbors
		     , buffer_id
		     , shm_notify
		     , &rval
	);

    int send_count = neighborhood_id->comm_buffer[buffer_id].local_send_count[idx];
    if (rval >= send_count)
    {
	ASSERT(rval <= send_count + 1);
	__sync_synchronize();
	id = idx;
    }

    return (id == -1) ? -1 : SHAN_SUCCESS;
}


int shan_comm_waitsome_remote(shan_neighborhood_t *const neighborhood_id
			      , int const type_id
			      , int const buffer_id
			      , int const idx
    )
{
    int id = -1;      
    int const iProcLocal    = neighborhood_id->iProcLocal;
    int const num_neighbors = neighborhood_id->num_neighbors;
    int const num_buffer    = neighborhood_id->num_buffer;

    int const sid = (neighborhood_id->comm_buffer[buffer_id].local_send_count[idx] - 1) % 2;  
    int const nid = GET_NOTIFICATION_ID(sid, num_buffer, buffer_id, num_neighbors, idx);  
    ASSERT(sid >= 0);
    ASSERT(nid >= 0);
    shan_remote_t *const remote_segment = &(neighborhood_id->remote_segment);

#ifdef USE_REMOTE_COMM_MPI

    int res, ret = 0;
    res = MPI_Test(&(neighborhood_id->comm_buffer[buffer_id].recv_req[sid*num_neighbors + idx])
                   , &ret
                   , MPI_STATUS_IGNORE
        );
    ASSERT(res == MPI_SUCCESS);
    if (ret != 0)
    {

#else

    gaspi_notification_id_t tmp_id;
    gaspi_notification_t nval;
    gaspi_return_t ret;
    if (( ret = gaspi_notify_waitsome (remote_segment->shan_id
				 , nid
				 , 1
				 , &tmp_id
				 , GASPI_TEST
	      )
	    ) == GASPI_SUCCESS)
    {
	SUCCESS_OR_DIE(gaspi_notify_reset (remote_segment->shan_id
					   , tmp_id
					   , &nval
			   )); 
	int const remote_rank = nval - 1;
	int const rank = neighborhood_id->neighbors[idx];
        ASSERT(rank == remote_rank);

#endif
	long buffer_offset = num_neighbors * neighborhood_id->comm_buffer[buffer_id].RecvOffset[sid]
            + idx * neighborhood_id->comm_buffer[buffer_id].maxRecvSz;

	void *comm_ptr = (char*) remote_segment->shan_ptr + buffer_offset;
	int *const comm_header    = (int *) comm_ptr;
	int const nelem_send      = *(comm_header);
	int const nelem_per_send  = *(comm_header + 1);
	int const rval            = *(comm_header + 2);	

	ASSERT(rval >= neighborhood_id->comm_buffer[buffer_id].local_send_count[idx]);
	ASSERT(rval <= neighborhood_id->comm_buffer[buffer_id].local_send_count[idx] + 1);

	type_local_t type_info;
	shan_get_shared_type(&type_info
			     , neighborhood_id
			     , iProcLocal
			     , neighborhood_id->num_neighbors
			     , type_id
	    );

#ifdef USE_VARIABLE_MESSAGE_LEN
	type_info.nelem_recv[idx]      = nelem_send;
	type_info.nelem_per_recv[idx]  = nelem_per_send;
#else
	ASSERT(type_info.nelem_recv[idx] == nelem_send);
	ASSERT(type_info.nelem_per_recv[idx] == nelem_per_send);
#endif
	id = idx;

    }

    return (id == -1) ? -1 : SHAN_SUCCESS;
}

static void shan_get_meta_data(void *data_ptr
			       , int nelem_send
			       , int nelem_per_send
			       , int nelem_recv
			       , int nelem_per_recv
			       , int *layout
			       , long *send_size
			       , long *recv_size
    )
{
    *layout               = shan_get_type_layout(data_ptr);
    int const type_sz     = shan_copy_get_type_sz(*layout);
    ASSERT(*layout / MAX_LAYOUTS >= 0);
    ASSERT(*layout / MAX_LAYOUTS <  2);

    long d1, d2;
    shan_get_array_dimension(data_ptr 
			     , &d1
			     , &d2);

    /*
     * set dimension, send_size, recv_size
     */
    *send_size  = nelem_send * nelem_per_send * d1 * type_sz 
	+ NELEM_COMM_HEADER * sizeof(int);		
    *recv_size  = nelem_recv * nelem_per_recv * d1 * type_sz 
	+ NELEM_COMM_HEADER * sizeof(int);		
}

int shan_comm_write(shan_neighborhood_t *const neighborhood_id
		    , shan_segment_t *const data_segment
		    , int type_id
		    , int buffer_id
		    , int idx
    )
{
    int iProcRemote;
    if ((iProcRemote = neighborhood_id->localRank[idx]) == -1)
    {
	int i, j;            
	int const iProcLocal    = neighborhood_id->iProcLocal;
	int const num_neighbors = neighborhood_id->num_neighbors;
	int const num_buffer    = neighborhood_id->num_buffer;
	
	type_local_t type_info;
	shan_get_shared_type(&type_info
			     , neighborhood_id
			     , iProcLocal
			     , num_neighbors
			     , type_id
	    );
    
	int layout;
	long send_size, recv_size;
	void *restrict data_ptr = data_segment->ptr_array[iProcLocal];
	shan_get_meta_data(data_ptr
			   , type_info.nelem_send[idx]
			   , type_info.nelem_per_send[idx]
			   , type_info.nelem_recv[idx]
			   , type_info.nelem_per_recv[idx]
			   , &layout
			   , &send_size
			   , &recv_size
	    );
	ASSERT(send_size <= neighborhood_id->comm_buffer[buffer_id].maxSendSz); 
	ASSERT(recv_size <= neighborhood_id->comm_buffer[buffer_id].maxRecvSz); 

	int const rank = neighborhood_id->neighbors[idx];
	int const sid  = (neighborhood_id->comm_buffer[buffer_id].local_send_count[idx]) % 2;    
	int const RemoteCommIdx = neighborhood_id->RemoteCommIndex[idx];
	int const RemoteNumNeighbors = neighborhood_id->RemoteNumNeighbors[idx];
    
	const long offset_local  = 
	    num_neighbors * neighborhood_id->comm_buffer[buffer_id].SendOffset[sid]
	    + idx * neighborhood_id->comm_buffer[buffer_id].maxSendSz;
      
	const long offset_remote = 
	    RemoteNumNeighbors * neighborhood_id->comm_buffer[buffer_id].RecvOffset[sid]
	    + RemoteCommIdx * neighborhood_id->comm_buffer[buffer_id].maxRecvSz;

	const int nid
	    = GET_NOTIFICATION_ID(sid, num_buffer, buffer_id, RemoteNumNeighbors, RemoteCommIdx);

#ifndef USE_REMOTE_COMM_MPI
	gaspi_number_t notification_num;
	SUCCESS_OR_DIE(gaspi_notification_num (&notification_num));
	ASSERT(nid < (int) notification_num);
#endif
      
	shan_remote_t *const remote_segment = &(neighborhood_id->remote_segment);
	void *restrict comm_ptr = (char*) remote_segment->shan_ptr + offset_local;
	void *restrict base_ptr = (char*) comm_ptr + NELEM_COMM_HEADER * sizeof(int);
	
	long *const send_index     = type_info.send_offset_index
	    + idx * neighborhood_id->type_element[type_id].max_nelem_send;

	long d1, d2;
	shan_get_array_dimension(data_ptr 
				 , &d1
				 , &d2);

	if (layout / MAX_LAYOUTS == 0)
	{    
	    shan_copy_src_comm_AoS(type_info.nelem_send[idx]
				   , type_info.nelem_per_send[idx]
				   , send_index 
				   , data_ptr
				   , base_ptr
				   , d1
				   , layout
		);
	}
	else if (layout / MAX_LAYOUTS == 1)
	{
	    shan_copy_src_comm_SoA(type_info.nelem_send[idx]
				   , type_info.nelem_per_send[idx]
				   , send_index 
				   , data_ptr
				   , base_ptr
				   , d1
				   , d2
				   , layout
		);
	}
	  
	int *const comm_header = (int *) ((char*) comm_ptr);
	*(comm_header)      = type_info.nelem_send[idx];
	*(comm_header + 1)  = type_info.nelem_per_send[idx];
	*(comm_header + 2)  = neighborhood_id->comm_buffer[buffer_id].local_send_count[idx] + 1;

#ifdef USE_REMOTE_COMM_MPI

	int res = MPI_Send(comm_ptr
			   , (int) send_size
			   , MPI_CHAR
			   , rank
			   , (int) nid
			   , neighborhood_id->MPI_COMM_ALL
	    );
	ASSERT(res == MPI_SUCCESS);

#else

	gaspi_queue_id_t queue_id = 0;
	write_notify_and_wait ( remote_segment->shan_id
				, (gaspi_offset_t) offset_local
				, rank
				, (gaspi_offset_t) offset_remote
				, (gaspi_size_t) send_size
				, (gaspi_notification_id_t) nid
				, (gaspi_notification_t) neighborhood_id->iProcGlobal + 1
				, queue_id
	    );
#endif

	++(neighborhood_id->comm_buffer[buffer_id].local_send_count[idx]);

    }

    return SHAN_SUCCESS;
}


int shan_comm_notify(shan_neighborhood_t *const neighborhood_id
		     , shan_segment_t *const data_segment
		     , int type_id
		     , int buffer_id
		     , int idx
    )

{

    int iProcRemote;
    if ((iProcRemote = neighborhood_id->localRank[idx]) != -1)
    {
	int const iProcLocal    = neighborhood_id->iProcLocal;
	int const num_neighbors = neighborhood_id->num_neighbors;
	
	type_local_t type_info;
	shan_get_shared_type(&type_info
			 , neighborhood_id
			 , iProcLocal
			 , num_neighbors
			 , type_id
	    );

	int layout;
	long send_size, recv_size;
	void *restrict data_ptr = data_segment->ptr_array[iProcLocal];     
	shan_get_meta_data(data_ptr
			   , type_info.nelem_send[idx]
			   , type_info.nelem_per_send[idx]
			   , type_info.nelem_recv[idx]
			   , type_info.nelem_per_recv[idx]
			   , &layout
			   , &send_size
			   , &recv_size
	    );
	ASSERT(send_size <= neighborhood_id->comm_buffer[buffer_id].maxSendSz); 
	ASSERT(recv_size <= neighborhood_id->comm_buffer[buffer_id].maxRecvSz); 
	
	int const shm_notify = neighborhood_id->num_neighbors * SHAN_NOTIFY + idx;
	shan_increment_local(neighborhood_id
			     , buffer_id
			     , shm_notify
	    );      	
	++(neighborhood_id->comm_buffer[buffer_id].local_send_count[idx]);
    }

    return SHAN_SUCCESS;

}



int shan_comm_post_recv(shan_neighborhood_t *const neighborhood_id
			, shan_segment_t *const data_segment
			, int type_id
			, int buffer_id
			, int idx
    )
{
#ifdef USE_REMOTE_COMM_MPI
    int iProcRemote;
    if ((iProcRemote = neighborhood_id->localRank[idx]) == -1)
    {
	int const iProcLocal    = neighborhood_id->iProcLocal;
	int const num_neighbors = neighborhood_id->num_neighbors;
	int const num_buffer    = neighborhood_id->num_buffer;
	
	type_local_t type_info;
	shan_get_shared_type(&type_info
			     , neighborhood_id
			     , iProcLocal
			     , num_neighbors
			     , type_id
	    );
	
	int layout;
	long send_size, recv_size;
	void *restrict data_ptr = data_segment->ptr_array[iProcLocal];
	shan_get_meta_data(data_ptr
			   , type_info.nelem_send[idx]
			   , type_info.nelem_per_send[idx]
			   , type_info.nelem_recv[idx]
			   , type_info.nelem_per_recv[idx]
			   , &layout
			   , &send_size
			   , &recv_size
	    );
	ASSERT(send_size <= neighborhood_id->comm_buffer[buffer_id].maxSendSz); 
	ASSERT(recv_size <= neighborhood_id->comm_buffer[buffer_id].maxRecvSz); 
	
	int const rank = neighborhood_id->neighbors[idx];
	int const sid  = (neighborhood_id->comm_buffer[buffer_id].local_send_count[idx]) % 2;    
	
	int const nid = GET_NOTIFICATION_ID(sid, num_buffer, buffer_id, num_neighbors, idx);
	long const recv_offset = num_neighbors * neighborhood_id->comm_buffer[buffer_id].RecvOffset[sid]
	    + idx * neighborhood_id->comm_buffer[buffer_id].maxRecvSz;
	
	shan_remote_t *const remote_segment = &(neighborhood_id->remote_segment);
	void *recv_ptr = (char*) remote_segment->shan_ptr + recv_offset;
	int res = MPI_Irecv(recv_ptr
			    , (int) recv_size
			    , MPI_CHAR
			    , rank
			    , nid
			    , neighborhood_id->MPI_COMM_ALL
			    , &(neighborhood_id->comm_buffer[buffer_id].recv_req[sid*num_neighbors + idx])
	    );

	ASSERT(res == MPI_SUCCESS);
    }
#endif
    return SHAN_SUCCESS;
}



void shan_comm_test4_request(shan_neighborhood_t *const neighborhood_id
			     , int buffer_id
			     , int idx
    )
{
#ifdef USE_REMOTE_COMM_MPI
    int iProcRemote;
    if ((iProcRemote = neighborhood_id->localRank[idx]) == -1)
    {
	int ret = 0;
	int const num_neighbors = neighborhood_id->num_neighbors;
	int const sid  = (neighborhood_id->comm_buffer[buffer_id].local_send_count[idx] - 1) % 2;    
	int const res = MPI_Test(&(neighborhood_id->comm_buffer[buffer_id].recv_req[sid*num_neighbors + idx])
				 , &ret
				 , MPI_STATUS_IGNORE
	    );
	ASSERT(res == MPI_SUCCESS);
    }
#endif

}
