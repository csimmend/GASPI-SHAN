var searchData=
[
  ['master',['master',['../structshan__neighborhood__t.html#a82b099408b246aaec24805ae58f782c6',1,'shan_neighborhood_t']]],
  ['max_5fnelem_5frecv',['max_nelem_recv',['../structshan__element__t.html#a63e06e116e876876ee9a91f901bc51aa',1,'shan_element_t']]],
  ['max_5fnelem_5fsend',['max_nelem_send',['../structshan__element__t.html#a027fc8c9e8f89eab051d11ef8a3f5cde',1,'shan_element_t']]],
  ['max_5fnum_5fneighbors',['max_num_neighbors',['../structshan__neighborhood__t.html#acfebaceb5524357f385888be68f168c0',1,'shan_neighborhood_t']]],
  ['maxrecvsz',['maxRecvSz',['../structshan__buffer__t.html#a3d53db452a516acf72c724bdc3c854bc',1,'shan_buffer_t']]],
  ['maxsendsz',['maxSendSz',['../structshan__buffer__t.html#ac057bbaa6d7cafece4251407d10f64d8',1,'shan_buffer_t']]],
  ['mpi_5fcomm_5fall',['MPI_COMM_ALL',['../structshan__neighborhood__t.html#a05e33890edbfea469b4190541740c98d',1,'shan_neighborhood_t']]],
  ['mpi_5fcomm_5fshm',['MPI_COMM_SHM',['../structshan__neighborhood__t.html#af7e2a992b3fb65b9860377d26792ee2d',1,'shan_neighborhood_t::MPI_COMM_SHM()'],['../structshan__segment__t.html#a0ef0eb1f9f3d377c39b54e847598e2d2',1,'shan_segment_t::MPI_COMM_SHM()']]]
];
