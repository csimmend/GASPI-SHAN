var searchData=
[
  ['f_5fshan_5falloc_5fget_5fdefault_5fmem_5fsz',['F_SHAN_ALLOC_GET_DEFAULT_MEM_SZ',['../interfacef__shan_1_1F__SHAN__ALLOC__GET__DEFAULT__MEM__SZ.html',1,'f_shan']]],
  ['f_5fshan_5falloc_5fshared',['F_SHAN_ALLOC_SHARED',['../interfacef__shan_1_1F__SHAN__ALLOC__SHARED.html',1,'f_shan']]],
  ['f_5fshan_5fcomm_5fnotify',['F_SHAN_COMM_NOTIFY',['../interfacef__shan_1_1F__SHAN__COMM__NOTIFY.html',1,'f_shan']]],
  ['f_5fshan_5fcomm_5fnotify_5for_5fwrite',['F_SHAN_COMM_NOTIFY_OR_WRITE',['../interfacef__shan_1_1F__SHAN__COMM__NOTIFY__OR__WRITE.html',1,'f_shan']]],
  ['f_5fshan_5fcomm_5fpost_5frecv',['F_SHAN_COMM_POST_RECV',['../interfacef__shan_1_1F__SHAN__COMM__POST__RECV.html',1,'f_shan']]],
  ['f_5fshan_5fcomm_5fwait4all',['F_SHAN_COMM_WAIT4ALL',['../interfacef__shan_1_1F__SHAN__COMM__WAIT4ALL.html',1,'f_shan']]],
  ['f_5fshan_5fcomm_5fwait4buffer',['F_SHAN_COMM_WAIT4BUFFER',['../interfacef__shan_1_1F__SHAN__COMM__WAIT4BUFFER.html',1,'f_shan']]],
  ['f_5fshan_5fcomm_5fwrite',['F_SHAN_COMM_WRITE',['../interfacef__shan_1_1F__SHAN__COMM__WRITE.html',1,'f_shan']]],
  ['f_5fshan_5ffree_5fcomm',['F_SHAN_FREE_COMM',['../interfacef__shan_1_1F__SHAN__FREE__COMM.html',1,'f_shan']]],
  ['f_5fshan_5ffree_5fshared',['F_SHAN_FREE_SHARED',['../interfacef__shan_1_1F__SHAN__FREE__SHARED.html',1,'f_shan']]],
  ['f_5fshan_5fget_5fsegment_5fid',['F_SHAN_GET_SEGMENT_ID',['../interfacef__shan_1_1F__SHAN__GET__SEGMENT__ID.html',1,'f_shan']]],
  ['f_5fshan_5finit_5fcomm',['F_SHAN_INIT_COMM',['../interfacef__shan_1_1F__SHAN__INIT__COMM.html',1,'f_shan']]],
  ['f_5fshan_5finit_5fgaspi',['F_SHAN_INIT_GASPI',['../interfacef__shan_1_1F__SHAN__INIT__GASPI.html',1,'f_shan']]],
  ['f_5fshan_5fset_5fdata_5flayout',['F_SHAN_SET_DATA_LAYOUT',['../interfacef__shan_1_1F__SHAN__SET__DATA__LAYOUT.html',1,'f_shan']]],
  ['f_5fshan_5ftrack_5fmalloc',['F_SHAN_TRACK_MALLOC',['../interfacef__shan_1_1F__SHAN__TRACK__MALLOC.html',1,'f_shan']]],
  ['f_5fshan_5ftrack_5fptr_5fname',['F_SHAN_TRACK_PTR_NAME',['../interfacef__shan_1_1F__SHAN__TRACK__PTR__NAME.html',1,'f_shan']]],
  ['f_5fshan_5ftype_5flayout',['F_SHAN_TYPE_LAYOUT',['../interfacef__shan_1_1F__SHAN__TYPE__LAYOUT.html',1,'f_shan']]],
  ['f_5fshan_5ftype_5fvalidate',['F_SHAN_TYPE_VALIDATE',['../interfacef__shan_1_1F__SHAN__TYPE__VALIDATE.html',1,'f_shan']]]
];
