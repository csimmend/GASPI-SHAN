var searchData=
[
  ['segment_5fid',['segment_id',['../structshan__layout__t.html#a3b224b39659915577c1e62bfad584fc4',1,'shan_layout_t']]],
  ['segment_5fwin',['segment_win',['../structshan__segment__t.html#a85a9c8d2cb0ec9e3ead5f0a79dfcead6',1,'shan_segment_t']]],
  ['send_5foffset_5findex',['send_offset_index',['../structtype__local__t.html#a9d1e47d6ad9a51f61b126114a8e863f2',1,'type_local_t']]],
  ['sendoffset',['SendOffset',['../structshan__buffer__t.html#a0c933664804792bb399371155d64c4ed',1,'shan_buffer_t']]],
  ['shan_5fdomain_5fname',['shan_domain_name',['../structshan__segment__t.html#aa5f870d8d68479790654e99c8eae5edd',1,'shan_segment_t']]],
  ['shan_5fid',['shan_id',['../structshan__remote__t.html#a1b23c142128b38ded6fbeebd87daad3e',1,'shan_remote_t::shan_id()'],['../structshan__segment__t.html#a7fc2a9c4ec895c92e874d61e9f5bd5f8',1,'shan_segment_t::shan_id()']]],
  ['shan_5fptr',['shan_ptr',['../structshan__remote__t.html#aa928d17d58a533646435dcff0435003e',1,'shan_remote_t::shan_ptr()'],['../structshan__segment__t.html#a7f498eb7e7642c3ef7527c46dba18aed',1,'shan_segment_t::shan_ptr()']]],
  ['shan_5ftype',['shan_type',['../structshan__segment__t.html#a4730e781a4ddde44a0d6d60b14abf100',1,'shan_segment_t']]],
  ['shared_5fsegment',['shared_segment',['../structshan__neighborhood__t.html#a6327fe11775d0b3ed3c256865f5cd52c',1,'shan_neighborhood_t']]],
  ['shmid',['shmid',['../structshan__segment__t.html#a8f89447942b146036e0de74fe8055047',1,'shan_segment_t']]]
];
